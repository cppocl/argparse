/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_ARGPARSE_HPP
#define OCL_GUARD_ARGPARSE_ARGPARSE_HPP

#include "internal/ArgParseSortedArray.hpp"
#include "internal/ArgParseCharConstants.hpp"
#include "internal/ArgStringToInt.hpp"
#include "internal/ArgParseStringUtility.hpp"
#include "internal/ArgParseHelp.hpp"
#include <cstddef>

namespace ocl
{

/// Arguments are handles in the following formats:
///
/// -x          Single hyphen and singe character.
/// --xx...     Two hyphens and two or more characters.
/// -x=y...     Single hyphen, singe character, equals symbol and value.
/// --xx=y...   Two hyphens, two or more characters, equals symbol and value.
template<typename CharType, typename SizeType = std::size_t>
class ArgParse
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef ArgParseCharConstants<char_type> char_constants_type;
    typedef ArgParseSortedArray<char_type> sorted_array_type;
    typedef ArgParseHelp<char_type, size_type> arg_parse_help_type;
    typedef typename arg_parse_help_type::help_array_type help_array_type;

    static size_type const MAX_SIZE_TYPE = ~static_cast<size_type>(0);

public:
    /// Either parse argc and argv in the constructor, or if they are 0 and null then
    /// use the Reset function to construct the arguments later.
    /// When short and long help options are not null, identify if the help option
    /// is specified within argv while parsing the arguments.
    /// If short and long help options are null then help is not supported,
    /// and no additional parsing is performed for help.
    ArgParse(int argc = 0, char_type** argv = nullptr) noexcept;

    ~ArgParse() noexcept;

    /// Get the argv string at a given index or null if out of range.
    char_type const* operator[](size_type index) const noexcept;

public:
    /// Get number of options (same as argc).
    size_type GetCount() const noexcept;

    /// Re-use ArgParse for another argc and argv.
    void Reset(int argc, char_type** argv) noexcept;

    /// Bind any arguments from the beginning remaining after binding any - or -- options.
    /// Any remaining argument beginning with '-' will cause Bind to return false.
    bool Bind(char_type const*& arg) noexcept;

    /// Bind a - or -- option to a boolean value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(bool& value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a string value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(char_type const*& value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a signed char value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(signed char& value,
              signed char default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a unsigned char value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(unsigned char& value,
              unsigned char default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a signed short value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(signed short& value,
              signed short default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a unsigned short value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(unsigned short& value,
              unsigned short default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a signed int value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(signed int& value,
              signed int default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a unsigned int value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(unsigned int& value,
              unsigned int default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a signed long value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(signed long& value,
              signed long default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a unsigned long value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(unsigned long& value,
              unsigned long default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a signed long long value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(signed long long& value,
              signed long long default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Bind a - or -- option to a unsigned long long value, removing the option after binding.
    /// If help is not null then this is added to an array of help options, in the order
    /// each Bind call was made.
    bool Bind(unsigned long long& value,
              unsigned long long default_value,
              char_type const* short_option,
              char_type const* long_option,
              char_type const* description = nullptr,
              char_type const* value_name = nullptr) noexcept;

    /// Get the first invalid argument or return null for no errors.
    char_type const* GetInvalidArgument() const noexcept;

    /// Add all the elements that mane up a complete help for -? or --help.
    /// program_name is the name of the executable that would be used.
    /// args are any mandatory or optional arguments that exclude starting with "-" or "--".
    ///      Note that mandatory arguments should be placed inside "<>", e.g. "<source>"
    ///      Note that optional arguments should be placed inside "[]", e.g. "[destination]"
    /// version can be any string representing the version, e.g. "1", "1.0", etc.
    /// license can be any license string, e.g. "Apache 2.0"
    /// description can be any string that will be word wrapped when output.
    /// example can be any string for example usage, which will be word wrapped when output.
    /// NOTE: All help is automatically word wrapped, but "\n" can be used to force line wrapping.
    void AddHelp(char_type const* program_name,
                 char_type const* args,
                 char_type const* version,
                 char_type const* license,
                 char_type const* description,
                 char_type const* example,
                 char_type const* short_help_option = "-?",
                 char_type const* long_help_option = "--help",
                 size_type max_row_length = MAX_SIZE_TYPE) noexcept;

    /// Return the next row of text for outputting the help.
    /// If there are no more rows then null is returned.
    help_array_type const& GetHelp() noexcept;

    static bool IsShortOption(char_type const* option) noexcept;
    static bool IsLongOption(char_type const* option) noexcept;

private:
    /// Find the option and remove it, if it's found.
    /// Return the matched option or null if not found.
    /// NOTE: description is mandatory to enforce well documented options.
    char_type const* RemoveOption(char_type const* short_option,
                                  char_type const* long_option,
                                  char_type const* description,
                                  char_type const* value_name = nullptr) noexcept;

    /// Get the value after the "=" or return null if there is no value.
    static char_type const* GetValue(char_type const* option) noexcept;

    /// Get the integer value after the "=" or return false option is null.
    /// If there is no = after the option the default value is used.
    template<typename IntType>
    static bool GetIntValue(IntType& value, IntType default_value, char_type const* option) noexcept;

private:
    int m_argc;
    char_type** m_argv;
    sorted_array_type m_sorted_argv;

    /// The first invalid option set by Bind(arg) when Bind returns false.
    char_type const* m_invalid_option;

    arg_parse_help_type m_help;
};

#include "internal/ArgParse.inl"

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_ARGPARSE_HPP

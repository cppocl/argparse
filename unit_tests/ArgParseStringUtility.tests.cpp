/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/ArgParseStringUtility.hpp"

TEST_MEMBER_FUNCTION(ArgParseStringUtility, GetLength, char_const_ptr)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::ArgParseStringUtility<char_type, size_type> string_utility_type;

    CHECK_TRUE(string_utility_type::GetLength(nullptr) == 0U);
    CHECK_TRUE(string_utility_type::GetLength("") == 0U);
    CHECK_TRUE(string_utility_type::GetLength("A") == 1U);
    CHECK_TRUE(string_utility_type::GetLength("AB") == 2U);
}

TEST_MEMBER_FUNCTION(ArgParseStringUtility, UnsafeGetLength, char_const_ptr)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::ArgParseStringUtility<char_type, size_type> string_utility_type;

    CHECK_TRUE(string_utility_type::UnsafeGetLength("") == 0U);
    CHECK_TRUE(string_utility_type::UnsafeGetLength("A") == 1U);
    CHECK_TRUE(string_utility_type::UnsafeGetLength("AB") == 2U);
}

TEST_MEMBER_FUNCTION(ArgParseStringUtility, Compare, char_const_ptr_char_const_ptr)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::ArgParseStringUtility<char_type, size_type> string_utility_type;

    CHECK_TRUE(string_utility_type::Compare(nullptr, nullptr) == 0);
    CHECK_TRUE(string_utility_type::Compare("", "") == 0);
    CHECK_TRUE(string_utility_type::Compare(nullptr, "") == -1);
    CHECK_TRUE(string_utility_type::Compare("", nullptr) == 1);
    CHECK_TRUE(string_utility_type::Compare("A", "A") == 0);
    CHECK_TRUE(string_utility_type::Compare("AB", "AB") == 0);
    CHECK_TRUE(string_utility_type::Compare("A", "B") == -1);
    CHECK_TRUE(string_utility_type::Compare("A", "a") == -1);
    CHECK_TRUE(string_utility_type::Compare("AB", "ABC") == -1);
    CHECK_TRUE(string_utility_type::Compare("B", "A") == 1);
    CHECK_TRUE(string_utility_type::Compare("a", "A") == 1);
    CHECK_TRUE(string_utility_type::Compare("ABC", "AB") == 1);
}

TEST_MEMBER_FUNCTION(ArgParseStringUtility, UnsafeCompare, char_const_ptr_char_const_ptr)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::ArgParseStringUtility<char_type, size_type> string_utility_type;

    CHECK_TRUE(string_utility_type::UnsafeCompare("", "") == 0);
    CHECK_TRUE(string_utility_type::UnsafeCompare("A", "A") == 0);
    CHECK_TRUE(string_utility_type::UnsafeCompare("AB", "AB") == 0);
    CHECK_TRUE(string_utility_type::UnsafeCompare("A", "B") == -1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("A", "a") == -1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("AB", "ABC") == -1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("B", "A") == 1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("a", "A") == 1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("ABC", "AB") == 1);
}

TEST_MEMBER_FUNCTION(ArgParseStringUtility, Compare, char_const_ptr_char_const_ptr_size_t)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::ArgParseStringUtility<char_type, size_type> string_utility_type;

    CHECK_TRUE(string_utility_type::Compare(nullptr, nullptr, 0) == 0);
    CHECK_TRUE(string_utility_type::Compare("", "", 0) == 0);
    CHECK_TRUE(string_utility_type::Compare(nullptr, "", 0) == -1);
    CHECK_TRUE(string_utility_type::Compare("", nullptr, 0) == 1);
    CHECK_TRUE(string_utility_type::Compare("A", "A", 1) == 0);
    CHECK_TRUE(string_utility_type::Compare("AB", "AB", 2) == 0);
    CHECK_TRUE(string_utility_type::Compare("A", "B", 1) == -1);
    CHECK_TRUE(string_utility_type::Compare("A", "a", 1) == -1);
    CHECK_TRUE(string_utility_type::Compare("AB", "ABC", 2) == 0);
    CHECK_TRUE(string_utility_type::Compare("AB", "ABC", 3) == -1);
    CHECK_TRUE(string_utility_type::Compare("B", "A", 1) == 1);
    CHECK_TRUE(string_utility_type::Compare("a", "A", 1) == 1);
    CHECK_TRUE(string_utility_type::Compare("ABC", "AB", 2) == 0);
    CHECK_TRUE(string_utility_type::Compare("ABC", "AB", 3) == 1);
}

TEST_MEMBER_FUNCTION(ArgParseStringUtility, UnsafeCompare, char_const_ptr_char_const_ptr_size_t)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::ArgParseStringUtility<char_type, size_type> string_utility_type;

    CHECK_TRUE(string_utility_type::UnsafeCompare("A", "A", 1) == 0);
    CHECK_TRUE(string_utility_type::UnsafeCompare("AB", "AB", 2) == 0);
    CHECK_TRUE(string_utility_type::UnsafeCompare("A", "B", 1) == -1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("A", "a", 1) == -1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("AB", "ABC", 2) == 0);
    CHECK_TRUE(string_utility_type::UnsafeCompare("AB", "ABC", 3) == -1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("B", "A", 1) == 1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("a", "A", 1) == 1);
    CHECK_TRUE(string_utility_type::UnsafeCompare("ABC", "AB", 2) == 0);
    CHECK_TRUE(string_utility_type::UnsafeCompare("ABC", "AB", 3) == 1);
}

TEST_MEMBER_FUNCTION(ArgParseStringUtility, Find, char_const_ptr_char)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::ArgParseStringUtility<char_type, size_type> string_utility_type;

    auto InvalidPosition = string_utility_type::InvalidPosition;

    CHECK_TRUE(string_utility_type::Find(nullptr, 'A') == InvalidPosition);
    CHECK_TRUE(string_utility_type::Find("", 'A') == InvalidPosition);
    CHECK_TRUE(string_utility_type::Find("A", 'A') == 0U);
    CHECK_TRUE(string_utility_type::Find("A", 'a') == InvalidPosition);
    CHECK_TRUE(string_utility_type::Find("AB", 'A') == 0U);
    CHECK_TRUE(string_utility_type::Find("AB", 'B') == 1U);
}

TEST_MEMBER_FUNCTION(ArgParseStringUtility, UnsafeFind, char_const_ptr_char)
{
    typedef char char_type;
    typedef std::size_t size_type;
    typedef ocl::ArgParseStringUtility<char_type, size_type> string_utility_type;

    auto InvalidPosition = string_utility_type::InvalidPosition;

    CHECK_TRUE(string_utility_type::UnsafeFind("", 'A') == InvalidPosition);
    CHECK_TRUE(string_utility_type::UnsafeFind("A", 'A') == 0U);
    CHECK_TRUE(string_utility_type::UnsafeFind("A", 'a') == InvalidPosition);
    CHECK_TRUE(string_utility_type::UnsafeFind("AB", 'A') == 0U);
    CHECK_TRUE(string_utility_type::UnsafeFind("AB", 'B') == 1U);
}

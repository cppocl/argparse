/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/ArgvEmulation.hpp"
#include "../ArgParse.hpp"

namespace
{
    char* Copy(const char* str)
    {
        return ocl::ArgvEmulation::Copy(str);
    }

    wchar_t* Copy(wchar_t const* str)
    {
        return ocl::ArgvEmulation::Copy(str);
    }

    char** CreateArgv(char const* str)
    {
        return ocl::ArgvEmulation::CreateArgv(str);
    }

    char** CreateArgv(char const* str1, char const* str2)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2);
    }

    char** CreateArgv(char const* str1, char const* str2, char const* str3)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2, str3);
    }

    char** CreateArgv(char const* str1, char const* str2, char const* str3, char const* str4)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2, str3, str4);
    }

    void DeleteArgv(char** ar, std::size_t size)
    {
        ocl::ArgvEmulation::DeleteArgv(ar, size);
    }

    wchar_t** CreateArgv(wchar_t const* str)
    {
        return ocl::ArgvEmulation::CreateArgv(str);
    }

    wchar_t** CreateArgv(wchar_t const* str1, wchar_t const* str2)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2);
    }

    wchar_t** CreateArgv(wchar_t const* str1, wchar_t const* str2, wchar_t const* str3)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2, str3);
    }

    wchar_t** CreateArgv(wchar_t const* str1, wchar_t const* str2, wchar_t const* str3, wchar_t const* str4)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2, str3, str4);
    }

    void DeleteArgv(wchar_t** ar, std::size_t size)
    {
        ocl::ArgvEmulation::DeleteArgv(ar, size);
    }
}

TEST_MEMBER_FUNCTION(ArgParse, GetCount, NA)
{
    typedef char char_type;

    {
        int argc = 2;
        char_type **argv = CreateArgv("-A");

        ocl::ArgParse<char_type> args(argc, argv);

        CHECK_TRUE(args.GetCount() == 2);

        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char_type **argv = CreateArgv("-A", "-B");

        ocl::ArgParse<char_type> args(argc, argv);

        CHECK_TRUE(args.GetCount() == 3);

        DeleteArgv(argv, argc);
    }
}

TEST_MEMBER_FUNCTION(ArgParse, Reset, NA)
{
    typedef char char_type;

    {
        int argc = 2;
        char_type **argv = CreateArgv("-A");

        ocl::ArgParse<char_type> args;
        args.Reset(argc, argv);

        CHECK_TRUE(args.GetCount() == 2);

        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char_type **argv = CreateArgv("-A", "-B");

        ocl::ArgParse<char_type> args;
        args.Reset(argc, argv);

        CHECK_TRUE(args.GetCount() == 3);

        DeleteArgv(argv, argc);
    }
}

TEST_MEMBER_FUNCTION(ArgParse, Bind, type_char_const_ptr_char_const_ptr)
{
    typedef char char_type;

    {
        int argc = 2;
        char_type **argv = CreateArgv("-A");

        ocl::ArgParse<char_type> args;
        args.Reset(argc, argv);

        bool a = false;
        CHECK_TRUE(args.Bind(a, "-A", nullptr));

        DeleteArgv(argv, argc);
    }

    {
        int argc = 2;
        char_type **argv = CreateArgv("-A=abc");

        ocl::ArgParse<char_type> args;
        args.Reset(argc, argv);

        char const* a = nullptr;
        CHECK_TRUE(args.Bind(a, "-A", nullptr));
        CHECK_TRUE(a != nullptr && StrCmp(a, "abc") == 0);

        DeleteArgv(argv, argc);
    }

    {
        int argc = 5;
        char_type **argv = CreateArgv("-A=abc", "--bb=2", "-D", "text");

        ocl::ArgParse<char_type> args;
        args.Reset(argc, argv);

        char_type const* a = nullptr;
        CHECK_TRUE(args.Bind(a, "-A", nullptr));
        CHECK_TRUE(a != nullptr && StrCmp(a, "abc") == 0);

        int b = 0;
        CHECK_TRUE(args.Bind(b, 1, nullptr, "--bb"));
        CHECK_TRUE(b == 2);

        int c = 0;
        CHECK_FALSE(args.Bind(c, 1, nullptr, "--cc"));
        CHECK_TRUE(c == 0);

        int d = 0;
        CHECK_TRUE(args.Bind(d, 1, "-D", nullptr));
        CHECK_TRUE(d == 1);

        char_type const* text = nullptr;
        CHECK_TRUE(args.Bind(text));
        CHECK_TRUE(text != nullptr && StrCmp(text, "text") == 0);

        text = nullptr;
        CHECK_FALSE(args.Bind(text));
        CHECK_TRUE(text == nullptr);

        DeleteArgv(argv, argc);
    }

    {
        int argc = 5;
        char_type **argv = CreateArgv("-A=abc", "--bb=2", "-D", "text");

        ocl::ArgParse<char_type> args;
        args.Reset(argc, argv);

        char_type const* a = nullptr;
        CHECK_TRUE(args.Bind(a, "-A", nullptr));
        CHECK_TRUE(a != nullptr && StrCmp(a, "abc") == 0);

        int c = 0;
        CHECK_FALSE(args.Bind(c, 1, nullptr, "--cc"));
        CHECK_TRUE(c == 0);

        int d = 0;
        CHECK_TRUE(args.Bind(d, 1, "-D", nullptr));
        CHECK_TRUE(d == 1);

        char_type const* text = nullptr;
        CHECK_TRUE(args.Bind(text));
        CHECK_TRUE(text != nullptr && StrCmp(text, "text") == 0);

        DeleteArgv(argv, argc);
    }
}

TEST_MEMBER_FUNCTION(ArgParse, GetHelp, NA)
{
    typedef char char_type;
    typedef ocl::ArgParse<char_type> arg_parse_type;

    char_type empty_line[1] = { static_cast<char_type>(0) };

    {
        int argc = 5;
        char_type **argv = CreateArgv("-A", "--bb", "--depth", "text");
        arg_parse_type arg_parse(argc, argv);

        bool a;
        bool bb;
        int depth;
        char_type const* text;
        CHECK_TRUE(arg_parse.Bind(a, "-A", nullptr, "A description"));
        CHECK_TRUE(arg_parse.Bind(bb, nullptr, "--bb", "bb description", "B"));
        CHECK_TRUE(arg_parse.Bind(depth, 1, "-d", "--depth", "depth description"));
        CHECK_TRUE(arg_parse.Bind(text));

        typename arg_parse_type::help_array_type const& ar = arg_parse.GetHelp();
        char const* line = ar[0];
        CHECK_TRUE(StrCmp(line, "-A           A description") == 0);

        line = ar[1];
        CHECK_TRUE(StrCmp(line, "    --bb=B   bb description") == 0);

        line = ar[2];
        CHECK_TRUE(StrCmp(line, "-d  --depth  depth description") == 0);
    }

    {
        int argc = 5;
        char_type **argv = CreateArgv("-A", "--bb", "--depth", "text");
        arg_parse_type arg_parse(argc, argv);

        char_type const* prog = "prog";
        char_type const* args = "destination source";
        char_type const* prog_args = "prog destination source";
        char_type const* version = "1.0";
        char_type const* license = "Apache 2.0";
        char_type const* description = "A program";
        char_type const* example = "prog c:\\dest c:\\src";

        bool a;
        bool bb;
        int depth;
        char_type const* text;
        arg_parse.AddHelp(prog, args, version, license, description, example);
        CHECK_TRUE(arg_parse.Bind(a, "-A", nullptr, "A description"));
        CHECK_TRUE(arg_parse.Bind(bb, nullptr, "--bb", "bb description", "B"));
        CHECK_TRUE(arg_parse.Bind(depth, 1, "-d", "--depth", "depth description"));
        CHECK_TRUE(arg_parse.Bind(text));

        typename arg_parse_type::help_array_type const& ar = arg_parse.GetHelp();
        arg_parse_type::size_type index = 0;

        char const* line = ar[index++];
        CHECK_TRUE(StrCmp(line, version) == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, empty_line) == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, license) == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, empty_line) == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, description) == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, empty_line) == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, prog_args) == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, empty_line) == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, "-A           A description") == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, "    --bb=B   bb description") == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, "-d  --depth  depth description") == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, empty_line) == 0);

        line = ar[index++];
        CHECK_TRUE(StrCmp(line, example) == 0);
    }
}

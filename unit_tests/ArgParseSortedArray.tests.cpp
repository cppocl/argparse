/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/ArgParseSortedArray.hpp"
#include "../internal/ArgStringToInt.hpp"
#include "../internal/ArgvEmulation.hpp"

namespace
{
    char* Copy(const char* str)
    {
        return ocl::ArgvEmulation::Copy(str);
    }

    wchar_t* Copy(wchar_t const* str)
    {
        return ocl::ArgvEmulation::Copy(str);
    }

    char** CreateArgv(char const* str)
    {
        return ocl::ArgvEmulation::CreateArgv(str);
    }

    char** CreateArgv(char const* str1, char const* str2)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2);
    }

    char** CreateArgv(char const* str1, char const* str2, char const* str3)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2, str3);
    }

    char** CreateArgv(char const* str1, char const* str2, char const* str3, char const* str4)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2, str3, str4);
    }

    void DeleteArgv(char** ar, std::size_t size)
    {
        ocl::ArgvEmulation::DeleteArgv(ar, size);
    }

    wchar_t** CreateArgv(wchar_t const* str)
    {
        return ocl::ArgvEmulation::CreateArgv(str);
    }

    wchar_t** CreateArgv(wchar_t const* str1, wchar_t const* str2)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2);
    }

    wchar_t** CreateArgv(wchar_t const* str1, wchar_t const* str2, wchar_t const* str3)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2, str3);
    }

    wchar_t** CreateArgv(wchar_t const* str1, wchar_t const* str2, wchar_t const* str3, wchar_t const* str4)
    {
        return ocl::ArgvEmulation::CreateArgv(str1, str2, str3, str4);
    }

    void DeleteArgv(wchar_t** ar, std::size_t size)
    {
        ocl::ArgvEmulation::DeleteArgv(ar, size);
    }
}

TEST_MEMBER_FUNCTION(ArgParseSortedArray, constructor, int_char_ptr_ptr)
{
    typedef char char_type;
    typedef ocl::ArgParseSortedArray<char_type> sorted_array_type;

    {
        int argc = 0;
        char** argv = nullptr;
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.GetArgc() == 0);
        CHECK_TRUE(sorted_array.GetArgv() == nullptr);
        CHECK_TRUE(sorted_array.GetAt(0U) == nullptr);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-A", "-B");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-A=1", "-B");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-A", "-B=2");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-A=1", "-B=2");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-B", "-A");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-B", "-A=1");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-B=2", "-A");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-B=2", "-A=1");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        DeleteArgv(argv, argc);
    }
}

TEST_MEMBER_FUNCTION(ArgParseSortedArray, Reset, int_char_ptr_ptr)
{
    typedef char char_type;
    typedef ocl::ArgParseSortedArray<char_type> sorted_array_type;

    {
        int argc = 0;
        char** argv = nullptr;
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 0);
        CHECK_TRUE(sorted_array.GetArgv() == nullptr);
        CHECK_TRUE(sorted_array.GetAt(0U) == nullptr);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-A", "-B");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-A=1", "-B");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-A", "-B=2");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-A=1", "-B=2");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-B", "-A");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-B", "-A=1");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-B=2", "-A");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 3;
        char** argv = CreateArgv("-B=2", "-A=1");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_FALSE(sorted_array.HasError());
        CHECK_TRUE(sorted_array.GetArgc() == 3);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        DeleteArgv(argv, argc);
    }

    {
        int argc = 4;
        char** argv = CreateArgv("-A", "-A", "-B");
        sorted_array_type sorted_array;

        CHECK_FALSE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.HasError());
        CHECK_TRUE(sorted_array.GetErrorPosition() != sorted_array_type::InvalidPosition);
        CHECK_TRUE(sorted_array.GetErrorPosition() == 2U);
    }
}

TEST_MEMBER_FUNCTION(ArgParseSortedArray, Remove, size_type)
{
    typedef char char_type;
    typedef ocl::ArgParseSortedArray<char_type> sorted_array_type;

    {
        int argc = 4;
        char** argv = CreateArgv("-A", "-B", "-C");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 4);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(2), "-C") == 0);

        char_type const* arg = sorted_array.Remove("-A");
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-B") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-C") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 2U);

        arg = sorted_array.Remove("-C");
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-C") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-B") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 1U);

        arg = sorted_array.Remove("-B");
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-B") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 0U);

        DeleteArgv(argv, argc);
    }

    {
        int argc = 4;
        char** argv = CreateArgv("-A", "-B", "-C");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 4);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(2), "-C") == 0);

        char_type const* arg = sorted_array.Remove("-B");
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-B") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-C") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 2U);

        arg = sorted_array.Remove("-A");
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-C") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 1U);

        arg = sorted_array.Remove("-C");
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-C") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 0U);

        DeleteArgv(argv, argc);
    }

    {
        int argc = 4;
        char** argv = CreateArgv("-A=1", "-B=2", "-C=3");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 4);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(2), "-C=3") == 0);

        char_type const* arg = sorted_array.Remove("-B");
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-B=2") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-C=3") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 2U);

        arg = sorted_array.Remove("-A");
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-C=3") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 1U);

        arg = sorted_array.Remove("-C");
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-C=3") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 0U);

        DeleteArgv(argv, argc);
    }
}

TEST_MEMBER_FUNCTION(ArgParseSortedArray, RemoveByPosition, size_type)
{
    typedef char char_type;
    typedef ocl::ArgParseSortedArray<char_type> sorted_array_type;

    {
        int argc = 4;
        char** argv = CreateArgv("-A", "-B", "-C");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 4);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(2), "-C") == 0);

        char_type const* arg = sorted_array.RemoveByPosition(0U);
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-B") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-C") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 2U);

        arg = sorted_array.RemoveByPosition(1U);
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-C") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-B") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 1U);

        arg = sorted_array.RemoveByPosition(0U);
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-B") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 0U);

        DeleteArgv(argv, argc);
    }

    {
        int argc = 4;
        char** argv = CreateArgv("-A", "-B", "-C");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 4);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(2), "-C") == 0);

        char_type const* arg = sorted_array.RemoveByPosition(1U);
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-B") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-C") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 2U);

        arg = sorted_array.RemoveByPosition(0U);
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-A") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-C") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 1U);

        arg = sorted_array.RemoveByPosition(0U);
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-C") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 0U);

        DeleteArgv(argv, argc);
    }

    {
        int argc = 4;
        char** argv = CreateArgv("-A=1", "-B=2", "-C=3");
        sorted_array_type sorted_array;

        CHECK_TRUE(sorted_array.Reset(argc, argv));
        CHECK_TRUE(sorted_array.GetArgc() == 4);
        CHECK_TRUE(sorted_array.GetArgv() != nullptr);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-B=2") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(2), "-C=3") == 0);

        char_type const* arg = sorted_array.RemoveByPosition(1U);
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-B=2") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(1), "-C=3") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 2U);

        arg = sorted_array.RemoveByPosition(0U);
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-A=1") == 0);
        CHECK_TRUE(StrCmp(sorted_array.GetAt(0), "-C=3") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 1U);

        arg = sorted_array.RemoveByPosition(0U);
        CHECK_TRUE(arg != nullptr && StrCmp(arg, "-C=3") == 0);
        CHECK_TRUE(sorted_array.GetSize() == 0U);

        DeleteArgv(argv, argc);
    }
}

TEST_MEMBER_FUNCTION(ArgParseSortedArray, Exists, char_const_ptr_size_t)
{
    typedef char char_type;
    typedef ocl::ArgParseSortedArray<char_type> sorted_array_type;

    {
        int argc = 4;
        char** argv = CreateArgv("-A", "-B", "-C");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.Exists("-A"));
        CHECK_TRUE(sorted_array.Exists("-B"));
        CHECK_TRUE(sorted_array.Exists("-C"));
        CHECK_FALSE(sorted_array.Exists("-D"));

        DeleteArgv(argv, argc);
    }

    {
        int argc = 4;
        char** argv = CreateArgv("-A=1", "-B=2", "-C=3");
        sorted_array_type sorted_array(argc, argv);

        CHECK_TRUE(sorted_array.Exists("-A"));
        CHECK_TRUE(sorted_array.Exists("-B"));
        CHECK_TRUE(sorted_array.Exists("-C"));
        CHECK_FALSE(sorted_array.Exists("-D"));

        DeleteArgv(argv, argc);
    }
}

TEST_MEMBER_FUNCTION(ArgParseSortedArray, FindPosition, char_const_ptr_size_t)
{
    typedef char char_type;
    typedef ocl::ArgParseSortedArray<char_type> sorted_array_type;
    typedef typename sorted_array_type::size_type size_type;
    size_type nearest_position = sorted_array_type::InvalidPosition;

    {
        int argc = 4;
        char** argv = CreateArgv("-A", "-B", "-C");
        sorted_array_type sorted_array(argc, argv);

        CHECK_FALSE(sorted_array.FindPosition(nearest_position, "-0") == 0U);
        CHECK_TRUE(nearest_position == 0U);
        CHECK_TRUE(sorted_array.FindPosition(nearest_position, "-A") == 0U);
        CHECK_TRUE(nearest_position == 0U);
        CHECK_TRUE(sorted_array.FindPosition(nearest_position, "-B") == 1U);
        CHECK_TRUE(nearest_position == 1U);
        CHECK_TRUE(sorted_array.FindPosition(nearest_position, "-C") == 2U);
        CHECK_TRUE(nearest_position == 2U);
        CHECK_FALSE(sorted_array.FindPosition(nearest_position, "-D") == 2U);
        CHECK_TRUE(nearest_position == 3U);

        DeleteArgv(argv, argc);
    }

    {
        int argc = 4;
        char** argv = CreateArgv("-A=1", "-B=2", "-C=3");
        sorted_array_type sorted_array(argc, argv);

        CHECK_FALSE(sorted_array.FindPosition(nearest_position, "-0") == 0U);
        CHECK_TRUE(nearest_position == 0U);
        CHECK_TRUE(sorted_array.FindPosition(nearest_position, "-A") == 0U);
        CHECK_TRUE(nearest_position == 0U);
        CHECK_TRUE(sorted_array.FindPosition(nearest_position, "-B") == 1U);
        CHECK_TRUE(nearest_position == 1U);
        CHECK_TRUE(sorted_array.FindPosition(nearest_position, "-C") == 2U);
        CHECK_TRUE(nearest_position == 2U);
        CHECK_FALSE(sorted_array.FindPosition(nearest_position, "-D") == 2U);
        CHECK_TRUE(nearest_position == 3U);

        DeleteArgv(argv, argc);
    }
}

/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/ArgStringToInt.hpp"
#include "../internal/ArgIntMinMax.hpp"
#include <cstdio>
#include <cwchar>

namespace
{
    template<typename IntType>
    char* IntToString(char* str, std::size_t str_len, char const* fmt, IntType value)
    {
        std::snprintf(str, str_len, fmt, value);
        return str;
    }

    template<typename IntType>
    wchar_t* IntToString(wchar_t* str, std::size_t str_len, wchar_t const* fmt, IntType value)
    {
        std::swprintf(str, str_len, fmt, value);
        return str;
    }
}

TEST_MEMBER_FUNCTION(ArgStringtoInt, ToInt, int_char_const_ptr)
{
    char str[100];

    {
        typedef signed char int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%d", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%d", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }

    {
        typedef unsigned char int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%d", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%d", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }

    {
        typedef signed short int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%d", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%d", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }

    {
        typedef unsigned short int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%d", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%d", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }

    {
        typedef signed int int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%d", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%d", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }

    {
        typedef unsigned int int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%u", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%u", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }

    {
        typedef signed long int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%ld", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%ld", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }

    {
        typedef unsigned long int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%lu", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%lu", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }

    {
        typedef signed long long int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%lld", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%lld", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }

    {
        typedef unsigned long long int_type;
        typedef ocl::ArgIntMinMax<int_type> min_max_type;

        int_type value = 0;
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "1"));
        CHECK_TRUE(value == 1);

        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, "0"));
        CHECK_TRUE(value == 0);

        IntToString(str, sizeof(str), "%llu", min_max_type::MIN_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MIN_INT);

        IntToString(str, sizeof(str), "%llu", min_max_type::MAX_INT);
        CHECK_TRUE(ocl::ArgStringToInt::ToInt(value, str));
        CHECK_TRUE(value == min_max_type::MAX_INT);
    }
}

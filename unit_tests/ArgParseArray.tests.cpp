/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../internal/ArgParseArray.hpp"

TEST_MEMBER_FUNCTION(ArgParseArray, constructor, NA)
{
    ocl::ArgParseArray<char> ar(10);
    CHECK_TRUE(ar.GetSize() == 0);
    CHECK_TRUE(ar.GetAllocSize() == 0);
    CHECK_TRUE(ar.GetGrowBy() == 10);
}

TEST_MEMBER_FUNCTION(ArgParseArray, Append, NA)
{
    ocl::ArgParseArray<char> ar(10);
    ar.Append('A');
    CHECK_TRUE(ar.GetSize() == 1U);
    CHECK_TRUE(ar.GetAllocSize() == 10U);
    CHECK_TRUE(ar[0] == 'A');

    ar.Append('B');
    CHECK_TRUE(ar.GetSize() == 2U);
    CHECK_TRUE(ar.GetAllocSize() == 10U);
    CHECK_TRUE(ar[0] == 'A');
    CHECK_TRUE(ar[1] == 'B');

    ar.Clear();
    CHECK_TRUE(ar.GetSize() == 0U);
}

TEST_MEMBER_FUNCTION(ArgParseArray, Move, NA)
{
    ocl::ArgParseArray<char> ar(10);
    ocl::ArgParseArray<char> ar2;
    ar.Append('A');
    ar2.Move(ar);
    CHECK_TRUE(ar.GetSize() == 0U);
    CHECK_TRUE(ar2.GetSize() == 1U);
    CHECK_TRUE(ar2.GetAllocSize() == 10U);
    CHECK_TRUE(ar2[0] == 'A');
}

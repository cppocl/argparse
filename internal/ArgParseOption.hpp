/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSEOPTION_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSEOPTION_HPP

#include "ArgParseArray.hpp"
#include "ArgParseStringUtility.hpp"
#include <cstddef>

namespace ocl
{

template<typename CharType, typename SizeType = std::size_t>
struct ArgParseOption
{
    typedef CharType char_type;
    typedef SizeType size_type;

    /// ArgParse only supports - with a single character.
    /// -- must be used with 2 or more characters for option name.
    static const size_type SHORT_OPTION_LEN = 2;

    /// Short option string of 2 characters, starting with "-".
    char_type const* short_option;

    /// Long option string of 4 or more characters, starting with "--".
    char_type const* long_option;

    /// Description that appears in the help for the options and value.
    char_type const* description;

    /// Name of the value that appears in the option for the help.
    /// E.g "[depth]" for --depth=[depth]
    char_type const* value_name;
};

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSEOPTION_HPP

/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSESTRINGUTILITY_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSESTRINGUTILITY_HPP

#include <cstring>
#include <cstddef>
#include <cstdlib>

namespace ocl
{

template<typename CharType, typename SizeType = std::size_t>
class ArgParseStringUtility
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;

    static char_type const NullChar = static_cast<char_type>(0);
    static char_type const SpaceChar = static_cast<char_type>(' ');
    static char_type const EqualChar = static_cast<char_type>('=');
    static size_type const InvalidPosition = ~static_cast<size_type>(0);

public:
    /// Get the length of the string or return 0 if string is null.
    static size_type GetLength(char_type const* str) noexcept
    {
        size_type len = 0;
        if (str != nullptr)
            for (; *str != NullChar; ++str)
                ++len;
        return len;
    }

    /// Get the length of the string or return 0 if string is null.
    static size_type UnsafeGetLength(char_type const* str) noexcept
    {
        size_type len = 0;
        for (; *str != NullChar; ++str)
            ++len;
        return len;
    }

    /// Compare two string, returning -1 if str1 < str2,
    /// returning 1 if str1 > str2, otherwise reutrn 0.
    static int Compare(char_type const* str1, char_type const* str2) noexcept
    {
        if (str1 != nullptr)
            return str2 != nullptr ? UnsafeCompare(str1, str2) : 1;
        return str2 != nullptr ? -1 : 0;
    }

    /// Compare two string, returning -1 if str1 < str2,
    /// returning 1 if str1 > str2, otherwise reutrn 0.
    static int Compare(char_type const* str1,
                       char_type const* str2,
                       size_type count) noexcept
    {
        if (str1 != nullptr)
            return str2 != nullptr ? UnsafeCompare(str1, str2, count) : 1;
        return str2 != nullptr ? -1 : 0;
    }

    /// Compare two strings and return true if they match.
    static int UnsafeCompare(char_type const* str1,
                             char_type const* str2) noexcept
    {
        while (*str1 != NullChar && *str1 == *str2)
        {
            ++str1;
            ++str2;
        }
        return *str1 < *str2 ? -1 : (*str1 > *str2 ? 1 : 0);
    }

    /// Compare two strings and return true if they match.
    static int UnsafeCompare(char_type const* str1,
                             char_type const* str2,
                             size_type count) noexcept
    {
        char_type const* str1_end = str1 + count;
        while (str1 < str1_end && *str1 == *str2)
        {
            ++str1;
            ++str2;
        }
        if (str1 == str1_end && count > 0)
        {
            --str1;
            --str2;
        }
        return *str1 < *str2 ? -1 : (*str1 > *str2 ? 1 : 0);
    }

    static size_type Find(char_type const* str, char_type char_to_find) noexcept
    {
        return str != nullptr ? UnsafeFind(str, char_to_find) : InvalidPosition;
    }

    static size_type Find(char_type const* str, size_type count, char_type char_to_find) noexcept
    {
        return str != nullptr ? UnsafeFind(str, count, char_to_find) : InvalidPosition;
    }

    static size_type FindLast(char_type const* str, char_type char_to_find) noexcept
    {
        return str != nullptr ? UnsafeFindLast(str, char_to_find) : InvalidPosition;
    }

    static size_type FindLast(char_type const* str, size_type count, char_type char_to_find) noexcept
    {
        return str != nullptr ? UnsafeFindLast(str, count, char_to_find) : InvalidPosition;
    }

    static size_type UnsafeFind(char_type const* str, char_type char_to_find) noexcept
    {
        for (char_type const* curr = str; *curr != NullChar; ++curr)
            if (*curr == char_to_find)
                return static_cast<size_type>(curr - str);
        return InvalidPosition;
    }

    static size_type UnsafeFind(char_type const* str, size_type count, char_type char_to_find) noexcept
    {
        char_type const* str_end = str + count;
        for (char_type const* curr = str; curr < str_end; ++curr)
            if (*curr == char_to_find)
                return static_cast<size_type>(curr - str);
        return InvalidPosition;
    }

    static size_type UnsafeFindLast(char_type const* str, char_type char_to_find) noexcept
    {
        size_type last_pos = InvalidPosition;
        for (char_type const* curr = str; *curr != NullChar; ++curr)
            if (*curr == char_to_find)
                last_pos = static_cast<size_type>(curr - str);
        return last_pos;
    }

    static size_type UnsafeFindLast(char_type const* str, size_type count, char_type char_to_find) noexcept
    {
        char_type const* str_end = str + count;
        for (char_type const* curr = str_end - 1; curr >= str; --curr)
            if (*curr == char_to_find)
                return static_cast<size_type>(curr - str);
        return InvalidPosition;
    }

    /// Fill string with a repeated character.
    static void Fill(char_type* str, char_type fill_char, size_type count) noexcept
    {
        if (str != nullptr)
            UnsafeFill(str, count, fill_char);
    }

    /// Fill string with a repeated character.
    static void UnsafeFill(char_type* str, char_type fill_char, size_type count) noexcept
    {
        char_type* str_end = str + count;
        while (str < str_end)
        {
            *str = fill_char;
            ++str;
        }
    }

    /// Copy str to dest.
    static void Copy(char_type* dest, char_type const* str, size_type count) noexcept
    {
        if (str != nullptr && dest != nullptr)
            UnsafeCopy(dest, str, count);
    }

    /// Copy str1 and str2 to dest.
    static void Copy(char_type* dest,
                     char_type const* str1, size_type count1,
                     char_type const* str2, size_type count2) noexcept
    {
        if (str1 != nullptr && str2 != nullptr && dest != nullptr)
            UnsafeCopy(dest, str1, count1, str2, count2);
    }

    /// Copy str1, str2 and str3 to dest.
    static void Copy(char_type* dest,
                     char_type const* str1, size_type count1,
                     char_type const* str2, size_type count2,
                     char_type const* str3, size_type count3) noexcept
    {
        if (str1 != nullptr && str2 != nullptr && str3 != nullptr && dest != nullptr)
            UnsafeCopy(dest, str1, count1, str2, count2, str3, count3);
    }

    /// Copy str1, str2, str3 and str4 to dest.
    static void Copy(char_type* dest,
                     char_type const* str1, size_type count1,
                     char_type const* str2, size_type count2,
                     char_type const* str3, size_type count3,
                     char_type const* str4, size_type count4) noexcept
    {
        if (str1 != nullptr && str2 != nullptr && str3 != nullptr && str4 != nullptr && dest != nullptr)
            UnsafeCopy(dest, str1, count1, str2, count2, str3, count3, str4, count4);
    }

    /// Copy str1, str2, str3, str4 and str5 to dest.
    static void Copy(char_type* dest,
                     char_type const* str1, size_type count1,
                     char_type const* str2, size_type count2,
                     char_type const* str3, size_type count3,
                     char_type const* str4, size_type count4,
                     char_type const* str5, size_type count5) noexcept
    {
        if (str1 != nullptr && str2 != nullptr && str3 != nullptr &&
            str4 != nullptr && str5 != nullptr && dest != nullptr)
        {
            UnsafeCopy(dest, str1, count1, str2, count2, str3, count3, str4, count4, str5, count5);
        }
    }

    /// Copy str to dest.
    static void UnsafeCopy(char_type* dest, char_type const* str, size_type count) noexcept
    {
        ::memcpy(dest, str, count * sizeof(char_type));
    }

    /// Copy str1 and str2 to dest.
    static void UnsafeCopy(char_type* dest,
                           char_type const* str1, size_type count1,
                           char_type const* str2, size_type count2) noexcept
    {
        ::memcpy(dest, str1, count1 * sizeof(char_type));
        ::memcpy(dest + count1, str2, count2 * sizeof(char_type));
    }

    /// Copy str1, str2 and str3 to dest.
    static void UnsafeCopy(char_type* dest,
                           char_type const* str1, size_type count1,
                           char_type const* str2, size_type count2,
                           char_type const* str3, size_type count3) noexcept
    {
        ::memcpy(dest, str1, count1 * sizeof(char_type));
        dest += count1;
        ::memcpy(dest, str2, count2 * sizeof(char_type));
        dest += count2;
        ::memcpy(dest, str3, count3 * sizeof(char_type));
    }

    /// Copy str1, str2, str3 and str4 to dest.
    static void UnsafeCopy(char_type* dest,
                           char_type const* str1, size_type count1,
                           char_type const* str2, size_type count2,
                           char_type const* str3, size_type count3,
                           char_type const* str4, size_type count4) noexcept
    {
        ::memcpy(dest, str1, count1 * sizeof(char_type));
        dest += count1;
        ::memcpy(dest, str2, count2 * sizeof(char_type));
        dest += count2;
        ::memcpy(dest, str3, count3 * sizeof(char_type));
        dest += count3;
        ::memcpy(dest, str4, count4 * sizeof(char_type));
    }

    /// Copy str1, str2, str3, str4 and str5 to dest.
    static void UnsafeCopy(char_type* dest,
                           char_type const* str1, size_type count1,
                           char_type const* str2, size_type count2,
                           char_type const* str3, size_type count3,
                           char_type const* str4, size_type count4,
                           char_type const* str5, size_type count5) noexcept
    {
        ::memcpy(dest, str1, count1 * sizeof(char_type));
        dest += count1;
        ::memcpy(dest, str2, count2 * sizeof(char_type));
        dest += count2;
        ::memcpy(dest, str3, count3 * sizeof(char_type));
        dest += count3;
        ::memcpy(dest, str4, count4 * sizeof(char_type));
        dest += count4;
        ::memcpy(dest, str5, count5 * sizeof(char_type));
    }

    static char_type* AllocCopyPadRight(char_type const* str,
                                       char_type pad_char,
                                       size_type pad_length) noexcept
    {
        return str != nullptr ? UnsafeAllocCopyPadRight(str, pad_length, pad_char) : nullptr;
    }

    /// Allocate a string, copy str and return the copy.
    static char_type* AllocCopy(char_type const* str) noexcept
    {
        return str != nullptr ? UnsafeAllocCopy(str, UnsafeGetLength(str) + 1) : nullptr;
    }

    /// Allocate a string, copy str1 and str2 and return the copy.
    static char_type* AllocCopy(char_type const* str1,
                                char_type const* str2) noexcept
    {
        return str1 != nullptr && str2 != nullptr
            ? UnsafeAllocCopy(str1, UnsafeGetLength(str1),
                              str2, UnsafeGetLength(str2) + 1)
            : nullptr;
    }

    /// Allocate a string, copy str1, str2 and str3 and return the copy.
    static char_type* AllocCopy(char_type const* str1,
                                char_type const* str2,
                                char_type const* str3) noexcept
    {
        return str1 != nullptr && str2 != nullptr && str3 != nullptr
            ? UnsafeAllocCopy(str1, UnsafeGetLength(str1),
                              str2, UnsafeGetLength(str2),
                              str3, UnsafeGetLength(str3) + 1)
            : nullptr;
    }

    /// Allocate a string, copy str1, str2, str3 and str4 and return the copy.
    static char_type* AllocCopy(char_type const* str1,
                                char_type const* str2,
                                char_type const* str3,
                                char_type const* str4) noexcept
    {
        return str1 != nullptr && str2 != nullptr && str3 != nullptr && str4 != nullptr
            ? UnsafeAllocCopy(str1, UnsafeGetLength(str1),
                              str2, UnsafeGetLength(str2),
                              str3, UnsafeGetLength(str3),
                              str4, UnsafeGetLength(str4) + 1)
            : nullptr;
    }

    /// Allocate a string, copy str1, str2, str3, str4 and str5 and return the copy.
    static char_type* AllocCopy(char_type const* str1,
                                char_type const* str2,
                                char_type const* str3,
                                char_type const* str4,
                                char_type const* str5) noexcept
    {
        return str1 != nullptr && str2 != nullptr && str3 != nullptr && str4 != nullptr && str5 != nullptr
            ? UnsafeAllocCopy(str1, UnsafeGetLength(str1),
                              str2, UnsafeGetLength(str2),
                              str3, UnsafeGetLength(str3),
                              str4, UnsafeGetLength(str4),
                              str5, UnsafeGetLength(str5) + 1)
            : nullptr;
    }

    static char_type* UnsafeAllocCopyPadRight(char_type const* str,
                                              char_type pad_char,
                                              size_type pad_length) noexcept
    {
        size_type len = UnsafeGetLength(str);
        if (pad_length < len)
            pad_length = len;
        char_type* new_str = static_cast<char_type*>(std::malloc(sizeof(char_type) * (pad_length + 1)));
        if (new_str != nullptr)
        {
            UnsafeCopy(new_str, str, len);
            if (pad_length > len)
                UnsafeFill(new_str + len, pad_char, pad_length - len);
            *(new_str + pad_length) = NullChar;
        }
        return new_str;
    }

    /// Allocate a string, copy str and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str) noexcept
    {
        return UnsafeAllocCopy(str, UnsafeGetLength(str) + 1);
    }

    /// Allocate a string, copy str and str2 and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str1,
                                      char_type const* str2) noexcept
    {
        return UnsafeAllocCopy(str1, UnsafeGetLength(str1),
                               str2, UnsafeGetLength(str2) + 1);
    }

    /// Allocate a string, copy str, str2 and str3 and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str1,
                                      char_type const* str2,
                                      char_type const* str3) noexcept
    {
        return UnsafeAllocCopy(str1, UnsafeGetLength(str1),
                               str2, UnsafeGetLength(str2),
                               str3, UnsafeGetLength(str3) + 1);
    }

    /// Allocate a string, copy str, str2, str3 and str4 and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str1,
                                      char_type const* str2,
                                      char_type const* str3,
                                      char_type const* str4) noexcept
    {
        return UnsafeAllocCopy(str1, UnsafeGetLength(str1),
                               str2, UnsafeGetLength(str2),
                               str3, UnsafeGetLength(str3),
                               str4, UnsafeGetLength(str4) + 1);
    }

    /// Allocate a string, copy str, str2, str3, str4 and str5 and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str1,
                                      char_type const* str2,
                                      char_type const* str3,
                                      char_type const* str4,
                                      char_type const* str5) noexcept
    {
        return UnsafeAllocCopy(str1, UnsafeGetLength(str1),
                               str2, UnsafeGetLength(str2),
                               str3, UnsafeGetLength(str3),
                               str3, UnsafeGetLength(str4),
                               str4, UnsafeGetLength(str5) + 1);
    }

    /// Allocate a string, copy str and return the copy.
    static char_type* AllocCopy(char_type const* str, size_type count) noexcept
    {
        return str != nullptr ? UnsafeAllocCopy(str, count)  : nullptr;
    }

    /// Allocate a string, copy str1 and str2 and return the copy.
    static char_type* AllocCopy(char_type const* str1, size_type count1,
                                char_type const* str2, size_type count2) noexcept
    {
        return str1 != nullptr && str2 != nullptr
            ? UnsafeAllocCopy(str1, count1, str2, count2)
            : nullptr;
    }

    /// Allocate a string, copy str1, str2 and str3 and return the copy.
    static char_type* AllocCopy(char_type const* str1, size_type count1,
                                char_type const* str2, size_type count2,
                                char_type const* str3, size_type count3) noexcept
    {
        return str1 != nullptr && str2 != nullptr && str3 != nullptr
            ? UnsafeAllocCopy(str1, count1, str2, count2, str3, count3)
            : nullptr;
    }

    /// Allocate a string, copy str1, str2, str3 and str4 and return the copy.
    static char_type* AllocCopy(char_type const* str1, size_type count1,
                                char_type const* str2, size_type count2,
                                char_type const* str3, size_type count3,
                                char_type const* str4, size_type count4) noexcept
    {
        return str1 != nullptr && str2 != nullptr && str3 != nullptr && str4 != nullptr
            ? UnsafeAllocCopy(str1, count1, str2, count2, str3, count3, str4, count4)
            : nullptr;
    }

    /// Allocate a string, copy str1, str2, str3, str4 and str5 and return the copy.
    static char_type* AllocCopy(char_type const* str1, size_type count1,
                                char_type const* str2, size_type count2,
                                char_type const* str3, size_type count3,
                                char_type const* str4, size_type count4,
                                char_type const* str5, size_type count5) noexcept
    {
        return str1 != nullptr && str2 != nullptr && str3 != nullptr && str4 != nullptr && str5 != nullptr
            ? UnsafeAllocCopy(str1, count1, str2, count2, str3, count3, str4, count4, str5, count5)
            : nullptr;
    }

    /// Allocate a string, copy str and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str, size_type count) noexcept
    {
        char_type* new_str = static_cast<char_type*>(std::malloc(sizeof(char_type) * count));
        if (new_str != nullptr)
            UnsafeCopy(new_str, str, count);
        return new_str;
    }

    /// Allocate a string, copy str1 and str2 and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str1, size_type count1,
                                      char_type const* str2, size_type count2) noexcept
    {
        char_type* new_str = static_cast<char_type*>(std::malloc(
                                                        sizeof(char_type) * (count1 + count2)));
        if (new_str != nullptr)
            UnsafeCopy(new_str, str1, count1, str2, count2);
        return new_str;
    }

    /// Allocate a string, copy str1, str2 and str3 and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str1, size_type count1,
                                      char_type const* str2, size_type count2,
                                      char_type const* str3, size_type count3) noexcept
    {
        char_type* new_str = static_cast<char_type*>(std::malloc(
                                                sizeof(char_type) * (count1 + count2 + count3)));
        if (new_str != nullptr)
            UnsafeCopy(new_str, str1, count1, str2, count2, str3, count3);
        return new_str;
    }

    /// Allocate a string, copy str1, str2, str3 and str4 and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str1, size_type count1,
                                      char_type const* str2, size_type count2,
                                      char_type const* str3, size_type count3,
                                      char_type const* str4, size_type count4) noexcept
    {
        char_type* new_str = static_cast<char_type*>(std::malloc(
                                         sizeof(char_type) * (count1 + count2 + count3 + count4)));
        if (new_str != nullptr)
            UnsafeCopy(new_str, str1, count1, str2, count2, str3, count3, str4, count4);
        return new_str;
    }

    /// Allocate a string, copy str1, str2, str3, str4 and str5 and return the copy.
    static char_type* UnsafeAllocCopy(char_type const* str1, size_type count1,
                                      char_type const* str2, size_type count2,
                                      char_type const* str3, size_type count3,
                                      char_type const* str4, size_type count4,
                                      char_type const* str5, size_type count5) noexcept
    {
        char_type* new_str = static_cast<char_type*>(std::malloc(
                                sizeof(char_type) * (count1 + count2 + count3 + count4 + count5)));
        if (new_str != nullptr)
            UnsafeCopy(new_str, str1, count1, str2, count2, str3, count3, str4, count4, str5, count5);
        return new_str;
    }

    static char_type* AllocCharString(char_type ch, size_type count) noexcept
    {
        char_type* new_str = static_cast<char_type*>(std::malloc(sizeof(char_type) * (count + 1)));
        if (new_str != nullptr)
            UnsafeFill(new_str, ch, count);
        return new_str;
    }
};

}

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSESTRINGUTILITY_HPP

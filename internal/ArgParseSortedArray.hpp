/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSESORTEDARRAY_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSESORTEDARRAY_HPP

#include <cstdlib>
#include <cstring>
#include <cstddef>

#include "ArgParseStringUtility.hpp"
#include "ArgParseCharConstants.hpp"
#include "ArgParseArray.hpp"

namespace ocl
{

/// Store array of sorted argv arguments.
template<typename CharType>
class ArgParseSortedArray
{
public:
    typedef CharType char_type;
    typedef std::size_t size_type;
    typedef ArgParseStringUtility<char_type, std::size_t> string_utility_type;
    typedef ArgParseCharConstants<char_type> char_constants_type;

    static constexpr size_type const InvalidPosition = ~static_cast<size_type>(0);

    ArgParseSortedArray(int argc = 0, char_type** argv = nullptr) noexcept;

    ~ArgParseSortedArray() noexcept;

    /// Get the argc value passed into constructor or reset.
    int GetArgc() const noexcept;

    /// Get the argv value passed into constructor or reset.
    char_type** GetArgv() noexcept;

    /// Get the size of the sorted arguments (Should be the same as argc).
    size_type GetSize() const noexcept;

    /// Return sorted argument at the specified index.
    char_type const* GetAt(size_type index) const noexcept;

    /// Reset the sorted array from argc and argv.
    bool Reset(int argc, char_type** argv) noexcept;

    /// Return the matched option and remove from internal sorted list.
    char_type const* Remove(char_type const* option,
                            size_type option_len = InvalidPosition) noexcept;

    /// Return the matched option and remove from internal sorted list.
    char_type const* RemoveByPosition(size_type position) noexcept;

    /// Get remaining unsorted argument, not starting with "-" or "--".
    char_type const* GetUnsorted() noexcept;

    /// Return true if the option exists
    bool Exists(char_type const* option, size_type option_len = InvalidPosition);

    /// Find the position for the matching option, otherwise return InvalidPosition
    /// and set nearest position.
    size_type FindPosition(size_type& nearest_position,
                           char_type const* option,
                           size_type option_len = InvalidPosition) noexcept;

    /// Return true when an error occurred from inserting a duplicate argument.
    bool HasError() const noexcept;

    /// Get the first error position when creating the sorted array,
    /// or InvalidPosition if there is no error.
    size_type GetErrorPosition() const noexcept;

private:
    /// Insert the option with any value into the sorted list.
    /// Used by Reset function.
    bool Insert(char_type const* option,
                size_type option_len = InvalidPosition) noexcept;

    /// Get the length of the - or -- option without the = or anything following =.
    static size_type GetOptionLength(char_type const* option) noexcept;

    static bool FindPosition(size_type& nearest_pos,
                             char_type const** sorted_array,
                             size_type array_size,
                             char_type const* option,
                             size_type option_len = InvalidPosition) noexcept;

private:
    int m_argc;
    char_type** m_argv;

    /// Size and sorted arguments from argv.
    size_type m_argv_array_size;
    char_type const** m_sorted_argv_array;

    /// Longest -- option (including --)
    size_type m_longest_option;

    size_type m_error_position;

    /// Get the current position of the unsorted argument from m_argv, not starting with "-" or "--".
    /// NOTE: m_unsorted_position starts at position 1 to avoid the executable name.
    size_type m_unsorted_position;
};

#include "ArgParseSortedArray.inl"

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSESORTEDARRAY_HPP

/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGINTTOFIXEDSIZETYPE_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGINTTOFIXEDSIZETYPE_HPP

#include <cstddef>
#include <cstdint>

namespace ocl
{

template<std::size_t const SizeOfType, bool const is_signed>
struct ArgIntToFixedSizeType;

template<>
struct ArgIntToFixedSizeType<1, true>
{
    typedef std::int8_t  type;
    typedef std::int8_t  signed_type;
    typedef std::uint8_t unsigned_type;
};

template<>
struct ArgIntToFixedSizeType<1, false>
{
    typedef std::uint8_t type;
    typedef std::int8_t  signed_type;
    typedef std::uint8_t unsigned_type;
};

template<>
struct ArgIntToFixedSizeType<2, true>
{
    typedef std::int16_t  type;
    typedef std::int16_t  signed_type;
    typedef std::uint16_t unsigned_type;
};

template<>
struct ArgIntToFixedSizeType<2, false>
{
    typedef std::uint16_t type;
    typedef std::int16_t  signed_type;
    typedef std::uint16_t unsigned_type;
};

template<>
struct ArgIntToFixedSizeType<4, true>
{
    typedef std::int32_t  type;
    typedef std::int32_t  signed_type;
    typedef std::uint32_t unsigned_type;
};

template<>
struct ArgIntToFixedSizeType<4, false>
{
    typedef std::uint32_t type;
    typedef std::int32_t  signed_type;
    typedef std::uint32_t unsigned_type;
};

template<>
struct ArgIntToFixedSizeType<8, true>
{
    typedef std::int64_t  type;
    typedef std::int64_t  signed_type;
    typedef std::uint64_t unsigned_type;
};

template<>
struct ArgIntToFixedSizeType<8, false>
{
    typedef std::uint64_t type;
    typedef std::int64_t  signed_type;
    typedef std::uint64_t unsigned_type;
};

} //namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGINTTOFIXEDSIZETYPE_HPP

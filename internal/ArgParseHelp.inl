/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType, typename SizeType>
ArgParseHelp<CharType, SizeType>::ArgParseHelp() noexcept
    : m_short_help_option(nullptr)
    , m_long_help_option(nullptr)
    , m_program_name(nullptr)
    , m_args(nullptr)
    , m_version(nullptr)
    , m_license(nullptr)
    , m_description(nullptr)
    , m_example(nullptr)
    , m_longest_option(0)
    , m_max_help_row_length(80)
{
}

template<typename CharType, typename SizeType>
ArgParseHelp<CharType, SizeType>::~ArgParseHelp() noexcept
{
}

template<typename CharType, typename SizeType>
void ArgParseHelp<CharType, SizeType>::AddHelp(char_type const* program_name,
                                               char_type const* args,
                                               char_type const* version,
                                               char_type const* license,
                                               char_type const* description,
                                               char_type const* example,
                                               char_type const* short_help_option,
                                               char_type const* long_help_option,
                                               size_type max_row_length) noexcept
{
    m_program_name = program_name;
    m_args = args;
    m_version = version;
    m_license = license;
    m_description = description;
    m_example = example;
    m_short_help_option = short_help_option;
    m_long_help_option = long_help_option;
    m_max_help_row_length = max_row_length;
}

template<typename CharType, typename SizeType>
typename ArgParseHelp<CharType, SizeType>::help_array_type const&
ArgParseHelp<CharType, SizeType>::GetHelp() noexcept
{
    ClearHelp();

    static char_type empty[1] = { string_utility_type::NullChar };
    static char_type space[2] = { string_utility_type::SpaceChar, string_utility_type::NullChar };
    char_type* line = nullptr;

    if (m_version != nullptr)
    {
        line = string_utility_type::UnsafeAllocCopy(m_version);
        m_help_lines.Append(line);
        line = string_utility_type::UnsafeAllocCopy(empty);
        m_help_lines.Append(line);
    }

    if (m_license != nullptr)
    {
        line = string_utility_type::UnsafeAllocCopy(m_license);
        m_help_lines.Append(line);
        line = string_utility_type::UnsafeAllocCopy(empty);
        m_help_lines.Append(line);
    }

    if (m_description != nullptr)
    {
        line = string_utility_type::UnsafeAllocCopy(m_description);
        m_help_lines.Append(line);
        line = string_utility_type::UnsafeAllocCopy(empty);
        m_help_lines.Append(line);
    }

    if (m_program_name != nullptr)
    {
        line = m_args != nullptr ? string_utility_type::UnsafeAllocCopy(m_program_name, space, m_args)
                                 : string_utility_type::UnsafeAllocCopy(m_program_name);
        m_help_lines.Append(line);
        line = string_utility_type::UnsafeAllocCopy(empty);
        m_help_lines.Append(line);
    }

    for (typename sorted_array_type::size_type index = 0; index < m_help_options_array.GetSize(); ++index)
    {
        option_type& option = m_help_options_array[index];
        line = GetOptionLine(option);
        m_help_lines.Append(line);
    }

    if (m_example != nullptr)
    {
        line = string_utility_type::UnsafeAllocCopy(empty);
        m_help_lines.Append(line);
        line = string_utility_type::UnsafeAllocCopy(m_example);
        m_help_lines.Append(line);
    }

    return m_help_lines;
}

template<typename CharType, typename SizeType>
CharType const* ArgParseHelp<CharType, SizeType>::GetProgramName() const noexcept
{
    return m_program_name;
}

template<typename CharType, typename SizeType>
CharType const* ArgParseHelp<CharType, SizeType>::GetArguments() const noexcept
{
}

template<typename CharType, typename SizeType>
CharType const* ArgParseHelp<CharType, SizeType>::GetVersion() const noexcept
{
}

template<typename CharType, typename SizeType>
CharType const* ArgParseHelp<CharType, SizeType>::GetLicense() const noexcept
{
}

template<typename CharType, typename SizeType>
void ArgParseHelp<CharType, SizeType>::ClearHelp() noexcept
{
    for (typename help_array_type::size_type index = 0; index < m_help_lines.GetSize(); ++index)
        std::free(m_help_lines[index]);
}

template<typename CharType, typename SizeType>
bool ArgParseHelp<CharType, SizeType>::AppendOption(char_type const* short_option,
                                                    char_type const* long_option,
                                                    char_type const* description,
                                                    char_type const* value_name) noexcept
{
    option_type option = { short_option, long_option, description, value_name };
    return AppendOption(option);
}

template<typename CharType, typename SizeType>
bool ArgParseHelp<CharType, SizeType>::AppendOption(option_type const& option) noexcept
{
    size_type opt_length = string_utility_type::GetLength(option.long_option);
    size_type value_length = string_utility_type::GetLength(option.value_name);

    size_type extra_char = value_length > 0 ? 1 : 0;
    size_type len = opt_length + value_length + extra_char;

    // Record length of long option + value name + extra space for '=' character.
    if (len > m_longest_option)
        m_longest_option = len;

    return m_help_options_array.Append(option);
}

template<typename CharType, typename SizeType>
CharType* ArgParseHelp<CharType, SizeType>::GetOptionLine(option_type const& option) noexcept
{
    size_type short_opt_len  = option.short_option != nullptr ? SHORT_OPTION_LEN : 0;
    size_type value_name_len = string_utility_type::GetLength(option.value_name);
    size_type long_opt_len   = string_utility_type::GetLength(option.long_option);
    size_type desc_len       = string_utility_type::GetLength(option.description);

    size_type len = short_opt_len +
                    HELP_SEPARATION_SPACES +
                    m_longest_option +
                    HELP_SEPARATION_SPACES +
                    desc_len;

    // Allocate extra space for =value 
    size_type extra_len = value_name_len > 0 ? value_name_len + 1 : 0;
    len += extra_len;

    char_type* line = static_cast<char_type*>(std::malloc((len + 1) * sizeof(char_type)));
    if (line == nullptr || m_longest_option < long_opt_len + extra_len)
        return nullptr;

    char_type* pos = line;

    char_type empty_short_option[SHORT_OPTION_LEN + 1];
    char_type const* short_option = option.short_option;
    if (short_opt_len == 0)
    {
        string_utility_type::UnsafeFill(empty_short_option,
                                        string_utility_type::SpaceChar,
                                        SHORT_OPTION_LEN);
        empty_short_option[SHORT_OPTION_LEN] = string_utility_type::NullChar;
        short_option = empty_short_option;
    }

    CopyShortOption(pos, short_option, SHORT_OPTION_LEN);

    if (long_opt_len > 0)
        CopyLongOption(pos, option.long_option, long_opt_len, option.value_name, value_name_len);

    if (desc_len > 0)
    {
        size_type pad_length = HELP_SEPARATION_SPACES + (m_longest_option - (extra_len + long_opt_len));
        string_utility_type::UnsafeFill(pos, string_utility_type::SpaceChar, pad_length);
        pos += pad_length;
        string_utility_type::UnsafeCopy(pos, option.description, desc_len);
        pos += desc_len;
    }

    *pos = string_utility_type::NullChar;

    return line;
}

template<typename CharType, typename SizeType>
void ArgParseHelp<CharType, SizeType>::GetOptionSplitLines(option_type const& /*option*/,
                                                           help_array_type& lines,
                                                           size_type /*max_row_length*/) noexcept
{
    for (size_type index = 0; index < lines.GetSize(); ++index)
        std::free(lines[index]);
    lines.Clear();

    // TODO: Add split long lines version.
}

template<typename CharType, typename SizeType>
void ArgParseHelp<CharType, SizeType>::CopyShortOption(char_type*& dest,
                                                       char_type const* short_option,
                                                       size_type short_option_len) noexcept
{
    string_utility_type::UnsafeCopy(dest, short_option, short_option_len);
    dest += short_option_len;
    string_utility_type::UnsafeFill(dest, string_utility_type::SpaceChar, HELP_SEPARATION_SPACES);
    dest += HELP_SEPARATION_SPACES;
}

template<typename CharType, typename SizeType>
void ArgParseHelp<CharType, SizeType>::CopyLongOption(char_type*& dest,
                                                      char_type const* long_option,
                                                      size_type long_option_len,
                                                      char_type const* value_name,
                                                      size_type value_name_len) noexcept
{
    string_utility_type::UnsafeCopy(dest, long_option, long_option_len);
    dest += long_option_len;
    if (value_name_len > 0)
    {
        *dest++ = string_utility_type::EqualChar;
        string_utility_type::UnsafeCopy(dest, value_name, value_name_len);
        dest += value_name_len;
    }
}

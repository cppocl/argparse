/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGINTMINMAX_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGINTMINMAX_HPP

#include "ArgIntToFixedSizeType.hpp"
#include "ArgFixedSizeIntMinMax.hpp"

namespace ocl
{

template<typename IntType>
struct ArgIntMinMax
{
    typedef typename ArgIntToFixedSizeType<sizeof(IntType), static_cast<IntType>(-1) < 0>::type type;
    typedef typename ArgIntToFixedSizeType<sizeof(IntType), static_cast<IntType>(-1) < 0>::signed_type signed_type;
    typedef typename ArgIntToFixedSizeType<sizeof(IntType), static_cast<IntType>(-1) < 0>::unsigned_type unsigned_type;

    static type const          INT_BITS  = ArgFixedSizeIntMinMax<type>::INT_BITS;
    static signed_type const   MINUS_ONE = ArgFixedSizeIntMinMax<type>::MINUS_ONE;
    static unsigned_type const UINT_BITS = ArgFixedSizeIntMinMax<type>::UINT_BITS;
    static bool const          IS_SIGNED = ArgFixedSizeIntMinMax<type>::IS_SIGNED;
    static unsigned_type const MIN_UINT  = ArgFixedSizeIntMinMax<type>::MIN_UINT;
    static unsigned_type const MAX_UINT  = ArgFixedSizeIntMinMax<type>::MAX_UINT;
    static signed_type const   MIN_SINT  = ArgFixedSizeIntMinMax<type>::MIN_SINT;
    static signed_type const   MAX_SINT  = ArgFixedSizeIntMinMax<type>::MAX_SINT;
    static type const          MIN_INT   = ArgFixedSizeIntMinMax<type>::MIN_INT;
    static type const          MAX_INT   = ArgFixedSizeIntMinMax<type>::MAX_INT;
};

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGINTMINMAX_HPP


/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSEHELP_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSEHELP_HPP

#include "ArgParseSortedArray.hpp"
#include "ArgParseCharConstants.hpp"
#include "ArgStringToInt.hpp"
#include "ArgParseStringUtility.hpp"
#include "ArgParseOption.hpp"
#include <cstddef>

namespace ocl
{

template<typename CharType, typename SizeType>
class ArgParseHelp
{
public:
    typedef CharType char_type;
    typedef SizeType size_type;
    typedef ArgParseCharConstants<char_type> char_constants_type;
    typedef ArgParseSortedArray<char_type> sorted_array_type;
    typedef ArgParseOption<char_type> option_type;
    typedef ArgParseStringUtility<char_type, size_type> string_utility_type;

    /// Type for array of options converted into strings.
    typedef ArgParseArray<char_type*, size_type> help_array_type;

    /// Type for array of help options.
    typedef ArgParseArray<option_type, size_type> help_options_array_type;

    /// Fixed length for all short options starting with "-", e.g. "-v"
    static size_type const SHORT_OPTION_LEN = 2;

    /// Number of characters for help line to separate short option, long option and description.
    static size_type const HELP_SEPARATION_SPACES = 2;

public:
    ArgParseHelp() noexcept;
    ~ArgParseHelp() noexcept;

    /// Return the next row of text for outputting the help.
    /// If there are no more rows then null is returned.
    help_array_type const& GetHelp() noexcept;

    /// Get the name of the program that appears for the help.
    char_type const* GetProgramName() const noexcept;

    /// Get mandatory and optional arguments (those not starting with "-" or "--")
    char_type const* GetArguments() const noexcept;

    /// Get the version string set by AddHelp.
    char_type const* GetVersion() const noexcept;

    /// Get the license string set by AddHelp.
    char_type const* GetLicense() const noexcept;

    /// Free the memory for the help lines.
    void ClearHelp() noexcept;

    /// Add the help information that appears before the help for each option.
    void AddHelp(char_type const* program_name,
                 char_type const* args,
                 char_type const* version,
                 char_type const* license,
                 char_type const* description,
                 char_type const* example,
                 char_type const* short_help_option = "-?",
                 char_type const* long_help_option = "--help",
                 size_type max_row_length = 80) noexcept;

    /// Add help for short and long option.
    bool AppendOption(char_type const* short_option,
                      char_type const* long_option,
                      char_type const* description,
                      char_type const* value_name = nullptr) noexcept;

    /// Get number of options in help.
    size_type GetSize() const noexcept;

private:
    /// Append a help for short and long option, and description with an optional value name.
    bool AppendOption(option_type const& option) noexcept;

    /// Get the help line for an option, which includes the short option, long option,
    /// value name and description, padded for alignment with all option lines.
    char_type* GetOptionLine(option_type const& option) noexcept;

    /// Get the help line for an option split over multiple lines,
    /// which includes the short option, long option, value name and description,
    /// padded for alignment with all option lines.
    void GetOptionSplitLines(option_type const& option,
                             help_array_type& lines,
                             size_type max_row_length = 80) noexcept;

    /// Help to copy short option and separator space to destination string.
    static void CopyShortOption(char_type*& dest,
                                char_type const* short_option,
                                size_type short_option_len) noexcept;

    /// Help to copy long option and separator space to destination string.
    static void CopyLongOption(char_type*& dest,
                               char_type const* long_option,
                               size_type long_option_len,
                               char_type const* value_name,
                               size_type value_name_len) noexcept;

private:
    /// Array of short and long options with help text.
    help_options_array_type m_help_options_array;

    /// Help lines to be returned by GetHelp function.
    help_array_type m_help_lines;

    /// Short help option, e.g. -?
    char_type const* m_short_help_option;

    /// Longest help option, e.g. --help
    char_type const* m_long_help_option;

    /// Name of the program, tool or utility at command line.
    char_type const* m_program_name;

    /// Details of arguments without "-" or "--" that are mandatory and optional.
    /// E.g. "<source> [destination]" would be mandatory source and optional destination.
    char_type const* m_args;

    /// Version string for output with help.
    /// E.g. "1", "1.0", etc.
    char_type const* m_version;

    /// License string for output with help.
    /// E.g. "Apache 2.0", etc.
    char_type const* m_license;

    /// General description of program or tool.
    /// This can contain "\n" to format multi-line output.
    char_type const* m_description;

    /// Example of how the program or tool is used.
    /// This can contain "\n" to format multi-line output.
    char_type const* m_example;

    /// String of spaces used to separate short option, long option and description
    /// when outputting help.
    char_type* m_spacer;

    /// Longest -- option including any value name length. (used for padding help)
    size_type m_longest_option;

    /// Maximum length of a help line before word wrapping is applied.
    size_type m_max_help_row_length;
};

#include "ArgParseHelp.inl"

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSEHELP_HPP

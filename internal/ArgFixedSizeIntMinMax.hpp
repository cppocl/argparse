/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGFIXEDSIZEINTMINMAX_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGFIXEDSIZEINTMINMAX_HPP

#include <cstdint>

namespace ocl
{

template<typename IntType>
struct ArgFixedSizeIntMinMax;

template<>
struct ArgFixedSizeIntMinMax<std::int8_t>
{
    typedef std::int8_t  int_type;
    typedef int_type     signed_type;
    typedef std::uint8_t unsigned_type;

    static int_type const      INT_BITS  = sizeof(int_type) * CHAR_BIT;
    static signed_type const   MINUS_ONE = static_cast<signed_type>(-1);
    static unsigned_type const UINT_BITS = sizeof(unsigned_type) * CHAR_BIT;
    static bool const          IS_SIGNED = static_cast<int_type>(-1) < 0;
    static unsigned_type const MIN_UINT  = static_cast<unsigned_type>(0U);
    static unsigned_type const MAX_UINT  = static_cast<unsigned_type>(0xffu);
    static signed_type const   MIN_SINT  = (static_cast<signed_type>(MAX_UINT >> 1) * MINUS_ONE) - 1;
    static signed_type const   MAX_SINT  = static_cast<signed_type>(MAX_UINT >> 1U);
    static int_type const      MIN_INT   = MIN_SINT;
    static int_type const      MAX_INT   = MAX_SINT;
};

template<>
struct ArgFixedSizeIntMinMax<std::uint8_t>
{
    typedef std::uint8_t int_type;
    typedef std::int8_t  signed_type;
    typedef int_type     unsigned_type;

    static int_type const      INT_BITS  = sizeof(int_type) * CHAR_BIT;
    static signed_type const   MINUS_ONE = static_cast<signed_type>(-1);
    static unsigned_type const UINT_BITS = sizeof(unsigned_type) * CHAR_BIT;
    static bool const          IS_SIGNED = static_cast<int_type>(-1) < 0;
    static unsigned_type const MIN_UINT  = static_cast<unsigned_type>(0U);
    static unsigned_type const MAX_UINT  = static_cast<unsigned_type>(0xffu);
    static signed_type const   MIN_SINT  = (static_cast<signed_type>(MAX_UINT >> 1) * MINUS_ONE) - 1;
    static signed_type const   MAX_SINT  = static_cast<signed_type>(MAX_UINT >> 1U);
    static int_type const      MIN_INT   = MIN_UINT;
    static int_type const      MAX_INT   = MAX_UINT;
};

template<>
struct ArgFixedSizeIntMinMax<std::int16_t>
{
    typedef std::int16_t  int_type;
    typedef int_type      signed_type;
    typedef std::uint16_t unsigned_type;

    /// Selecting bit sized max unsigned value is done for short to work around Microsoft C++ C4245 compiler warning.

    static int_type const      INT_BITS   = sizeof(int_type) * CHAR_BIT;
    static signed_type const   MINUS_ONE  = static_cast<signed_type>(-1);
    static unsigned_type const UINT_BITS  = sizeof(unsigned_type) * CHAR_BIT;
    static bool const          IS_SIGNED  = static_cast<int_type>(-1) < 0;
    static unsigned_type const MIN_UINT   = static_cast<unsigned_type>(0U);
    static unsigned_type const MAX_UINT   = static_cast<std::uint16_t>(0xffffu);
    static signed_type const   MIN_SINT   = (static_cast<signed_type>(MAX_UINT >> 1) * MINUS_ONE) - 1;
    static signed_type const   MAX_SINT   = static_cast<signed_type>(MAX_UINT >> 1U);
    static int_type const      MIN_INT    = MIN_SINT;
    static int_type const      MAX_INT    = MAX_SINT;
};

template<>
struct ArgFixedSizeIntMinMax<std::uint16_t>
{
    typedef std::uint16_t int_type;
    typedef std::int16_t  signed_type;
    typedef int_type      unsigned_type;

    static int_type const      INT_BITS   = sizeof(int_type) * CHAR_BIT;
    static signed_type const   MINUS_ONE  = static_cast<signed_type>(-1);
    static unsigned_type const UINT_BITS  = sizeof(unsigned_type) * CHAR_BIT;
    static bool const          IS_SIGNED  = static_cast<int_type>(-1) < 0;
    static unsigned_type const MIN_UINT   = static_cast<unsigned_type>(0U);
    static unsigned_type const MAX_UINT   = static_cast<std::uint16_t>(0xffffu);
    static signed_type const   MIN_SINT   = (static_cast<signed_type>(MAX_UINT >> 1) * MINUS_ONE) - 1;
    static signed_type const   MAX_SINT   = static_cast<signed_type>(MAX_UINT >> 1U);
    static int_type const      MIN_INT    = MIN_UINT;
    static int_type const      MAX_INT    = MAX_UINT;
};

template<>
struct ArgFixedSizeIntMinMax<std::int32_t>
{
    typedef std::int32_t  int_type;
    typedef int_type      signed_type;
    typedef std::uint32_t unsigned_type;

    static int_type const      INT_BITS  = sizeof(int_type) * CHAR_BIT;
    static signed_type const   MINUS_ONE = static_cast<signed_type>(-1);
    static unsigned_type const UINT_BITS = sizeof(unsigned_type) * CHAR_BIT;
    static bool const          IS_SIGNED = static_cast<int_type>(-1) < 0;
    static unsigned_type const MIN_UINT  = static_cast<unsigned_type>(0U);
    static unsigned_type const MAX_UINT  = ~static_cast<unsigned_type>(0U);
    static signed_type const   MIN_SINT  = (static_cast<signed_type>(MAX_UINT >> 1) * MINUS_ONE) - 1;
    static signed_type const   MAX_SINT  = static_cast<signed_type>(MAX_UINT >> 1U);
    static int_type const      MIN_INT   = MIN_SINT;
    static int_type const      MAX_INT   = MAX_SINT;
};

template<>
struct ArgFixedSizeIntMinMax<std::uint32_t>
{
    typedef std::uint32_t int_type;
    typedef std::int32_t  signed_type;
    typedef int_type      unsigned_type;

    static int_type const      INT_BITS  = sizeof(int_type) * CHAR_BIT;
    static signed_type const   MINUS_ONE = static_cast<signed_type>(-1);
    static unsigned_type const UINT_BITS = sizeof(unsigned_type) * CHAR_BIT;
    static bool const          IS_SIGNED = static_cast<int_type>(-1) < 0;
    static unsigned_type const MIN_UINT  = static_cast<unsigned_type>(0U);
    static unsigned_type const MAX_UINT  = ~static_cast<unsigned_type>(0U);
    static signed_type const   MIN_SINT  = (static_cast<signed_type>(MAX_UINT >> 1) * MINUS_ONE) - 1;
    static signed_type const   MAX_SINT  = static_cast<signed_type>(MAX_UINT >> 1U);
    static int_type const      MIN_INT   = MIN_UINT;
    static int_type const      MAX_INT   = MAX_UINT;
};

template<>
struct ArgFixedSizeIntMinMax<std::int64_t>
{
    typedef std::int64_t  int_type;
    typedef int_type      signed_type;
    typedef std::uint64_t unsigned_type;

    static int_type const      INT_BITS  = sizeof(int_type) * CHAR_BIT;
    static signed_type const   MINUS_ONE = static_cast<signed_type>(-1);
    static unsigned_type const UINT_BITS = sizeof(unsigned_type) * CHAR_BIT;
    static bool const          IS_SIGNED = static_cast<int_type>(-1) < 0;
    static unsigned_type const MIN_UINT  = static_cast<unsigned_type>(0U);
    static unsigned_type const MAX_UINT  = ~static_cast<unsigned_type>(0U);
    static signed_type const   MIN_SINT  = (static_cast<signed_type>(MAX_UINT >> 1) * MINUS_ONE) - 1;
    static signed_type const   MAX_SINT  = static_cast<signed_type>(MAX_UINT >> 1U);
    static int_type const      MIN_INT   = MIN_SINT;
    static int_type const      MAX_INT   = MAX_SINT;
};

template<>
struct ArgFixedSizeIntMinMax<std::uint64_t>
{
    typedef std::uint64_t int_type;
    typedef std::int64_t  signed_type;
    typedef int_type      unsigned_type;

    static int_type const      INT_BITS  = sizeof(int_type) * CHAR_BIT;
    static signed_type const   MINUS_ONE = static_cast<signed_type>(-1);
    static unsigned_type const UINT_BITS = sizeof(unsigned_type) * CHAR_BIT;
    static bool const          IS_SIGNED = static_cast<int_type>(-1) < 0;
    static unsigned_type const MIN_UINT  = static_cast<unsigned_type>(0U);
    static unsigned_type const MAX_UINT  = ~static_cast<unsigned_type>(0U);
    static signed_type const   MIN_SINT  = (static_cast<signed_type>(MAX_UINT >> 1) * MINUS_ONE) - 1;
    static signed_type const   MAX_SINT  = static_cast<signed_type>(MAX_UINT >> 1U);
    static int_type const      MIN_INT   = MIN_UINT;
    static int_type const      MAX_INT   = MAX_UINT;
};

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGFIXEDSIZEINTMINMAX_HPP

/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGSTRINGTOLONGLONG_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGSTRINGTOLONGLONG_HPP

#include <cstdlib>
#include <cerrno>
#include <cwchar>

namespace ocl
{

template<typename CharType, bool const is_signed>
struct ArgStringToLongLong;

template<>
struct ArgStringToLongLong<char, true>
{
    typedef char char_type;
    typedef signed long long long_long_type;

    static bool ToLongLong(long_long_type& value, char_type const* str, int base = 10)
    {
        bool success = false;
        if (str != nullptr)
        {
            char_type* end = nullptr;
            errno = 0;
            value = std::strtoll(str, &end, base);
            if (value == LLONG_MIN || value == LLONG_MAX)
            {
                if (errno != ERANGE)
                    success = true;
            }
            else if (value == 0LL)
            {
                if (*str == '0')
                    success = true;
            }
            else
                success = true;
        }
        return success;
    }
};

template<>
struct ArgStringToLongLong<char, false>
{
    typedef char char_type;
    typedef unsigned long long long_long_type;

    static bool ToLongLong(long_long_type& value, char_type const* str, int base = 10)
    {
        bool success = false;
        if (str != nullptr)
        {
            char_type* end = nullptr;
            errno = 0;
            value = std::strtoull(str, &end, base);
            if (value == ULLONG_MAX)
            {
                if (errno != ERANGE)
                    success = true;
            }
            else if (value == 0LL)
            {
                if (*str == '0')
                    success = true;
            }
            else
                success = true;
        }
        return success;
    }
};

template<>
struct ArgStringToLongLong<wchar_t, true>
{
    typedef wchar_t char_type;
    typedef signed long long long_long_type;

    static bool ToLongLong(long_long_type& value, char_type const* str, int base = 10)
    {
        bool success = false;
        if (str != nullptr)
        {
            char_type* end = nullptr;
            errno = 0;
            value = std::wcstoll(str, &end, base);
            if (value == LLONG_MIN || value == LLONG_MAX)
            {
                if (errno != ERANGE)
                    success = true;
            }
            else if (value == 0LL)
            {
                if (*str == '0')
                    success = true;
            }
            else
                success = true;
        }
        return success;
    }
};

template<>
struct ArgStringToLongLong<wchar_t, false>
{
    typedef wchar_t char_type;
    typedef unsigned long long long_long_type;

    static bool ToLongLong(long_long_type& value, char_type const* str, int base = 10)
    {
        bool success = false;
        if (str != nullptr)
        {
            char_type* end = nullptr;
            errno = 0;
            value = std::wcstoull(str, &end, base);
            if (value == ULLONG_MAX)
            {
                if (errno != ERANGE)
                    success = true;
            }
            else if (value == 0LL)
            {
                if (*str == '0')
                    success = true;
            }
            else
                success = true;
        }
        return success;
    }
};

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGSTRINGTOLONGLONG_HPP

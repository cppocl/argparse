/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType, typename SizeType>
ArgParse<CharType, SizeType>::ArgParse(int argc, char_type** argv) noexcept
    : m_argc(argc)
    , m_argv(argv)
    , m_invalid_option(nullptr)
{
    Reset(argc, argv);
}

template<typename CharType, typename SizeType>
ArgParse<CharType, SizeType>::~ArgParse() noexcept
{
}

template<typename CharType, typename SizeType>
CharType const* ArgParse<CharType, SizeType>::operator[](size_type index) const noexcept
{
    return (index < static_cast<size_type>(m_argc)) ? m_argv[index] : nullptr;
}

template<typename CharType, typename SizeType>
SizeType ArgParse<CharType, SizeType>::GetCount() const noexcept
{
    return static_cast<size_type>(m_argc);
}

template<typename CharType, typename SizeType>
void ArgParse<CharType, SizeType>::Reset(int argc, char_type** argv) noexcept
{
    m_argc = argc;
    m_argv = argv;
    m_sorted_argv.Reset(argc, argv);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(char_type const*& arg) noexcept
{
    arg = m_sorted_argv.GetUnsorted();
    return arg != nullptr;
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(bool& value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    value = option != nullptr && GetValue(option) == nullptr;
    return value;
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(char_type const*& value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    value = GetValue(option);
    return option != nullptr;
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(signed char& value,
                                        signed char default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(unsigned char& value,
                                        unsigned char default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(signed short& value,
                                        signed short default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(unsigned short& value,
                                        unsigned short default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(signed int& value,
                                        signed int default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(unsigned int& value,
                                        unsigned int default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(signed long& value,
                                        signed long default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(unsigned long& value,
                                        unsigned long default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(signed long long& value,
                                        signed long long default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::Bind(unsigned long long& value,
                                        unsigned long long default_value,
                                        char_type const* short_option,
                                        char_type const* long_option,
                                        char_type const* description,
                                        char_type const* value_name) noexcept
{
    char_type const* option = RemoveOption(short_option, long_option, description, value_name);
    return GetIntValue(value, default_value, option);
}

template<typename CharType, typename SizeType>
CharType const* ArgParse<CharType, SizeType>::GetInvalidArgument() const noexcept
{
    return m_invalid_option;
}

template<typename CharType, typename SizeType>
void ArgParse<CharType, SizeType>::AddHelp(char_type const* program_name,
                                           char_type const* args,
                                           char_type const* version,
                                           char_type const* license,
                                           char_type const* description,
                                           char_type const* example,
                                           char_type const* short_help_option,
                                           char_type const* long_help_option,
                                           size_type max_row_length) noexcept
{
    m_help.AddHelp(program_name, args, version, license, description, example,
                   short_help_option, long_help_option, max_row_length);
}

template<typename CharType, typename SizeType>
typename ArgParse<CharType, SizeType>::help_array_type const&
ArgParse<CharType, SizeType>::GetHelp() noexcept
{
    return m_help.GetHelp();
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::IsShortOption(char_type const* option) noexcept
{
    return option != nullptr &&
           *option == char_constants_type::Hyphen &&
           *(option+1) != char_constants_type::Hyphen;
}

template<typename CharType, typename SizeType>
bool ArgParse<CharType, SizeType>::IsLongOption(char_type const* option) noexcept
{
    return option != nullptr &&
           *option == char_constants_type::Hyphen &&
           *(option+1) == char_constants_type::Hyphen &&
           *(option+2) != char_constants_type::Hyphen;
}

template<typename CharType, typename SizeType>
CharType const* ArgParse<CharType, SizeType>::RemoveOption(char_type const* short_option,
                                                           char_type const* long_option,
                                                           char_type const* description,
                                                           char_type const* value_name) noexcept
{
    char_type const* option = nullptr;

    if (description == nullptr ||
        m_help.AppendOption(short_option, long_option, description, value_name))
    {
        option = IsShortOption(short_option) ? m_sorted_argv.Remove(short_option) : nullptr;
        if (option == nullptr)
            option = IsLongOption(long_option) ? m_sorted_argv.Remove(long_option) : nullptr;
    }

    return option;
}

template<typename CharType, typename SizeType>
CharType const* ArgParse<CharType, SizeType>::GetValue(char_type const* option) noexcept
{
    char_type const* value = nullptr;
    if (option != nullptr)
    {
        while (*option != char_constants_type::Null)
        {
            if (*option == char_constants_type::Equal)
                break;
            ++option;
        }
        value = (*option == char_constants_type::Null) ? nullptr : option + 1;
    }
    return value;
}

template<typename CharType, typename SizeType>
template<typename IntType>
bool ArgParse<CharType, SizeType>::GetIntValue(IntType& value,
                                               IntType default_value,
                                               char_type const* option) noexcept
{
    char_type const* value_str = GetValue(option);
    if (value_str != nullptr)
        return ArgStringToInt::ToInt(value, value_str);
    else if (option != nullptr)
    {
        value = default_value;
        return true;
    }
    return false;
}

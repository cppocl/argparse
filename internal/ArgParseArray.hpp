/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSEARRAY_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSEARRAY_HPP

#include <cstdlib>

namespace ocl
{

/// Custom unsorted array which only supports append and indexing for supporting arg parse help.
template<typename Type, typename SizeType = std::size_t>
class ArgParseArray
{
public:
    typedef Type type;
    typedef SizeType size_type;

public:
    ArgParseArray(size_type grow_by = 1) noexcept
        : m_size(0)
        , m_alloc_size(0)
        , m_grow_by(grow_by)
        , m_array(nullptr)
    {
    }

    ArgParseArray(ArgParseArray<type, size_type>&& ar) noexcept
        : m_array(nullptr)
    {
        Move(ar);
    }

    ArgParseArray<type, size_type>& operator =(ArgParseArray<type, size_type>&& ar) noexcept
    {
        Move(ar);
        return *this;
    }

    ArgParseArray(ArgParseArray<type, size_type> const& ar) = delete;
    ArgParseArray<type, size_type>& operator =(ArgParseArray<type, size_type> const& ar) = delete;

    ~ArgParseArray() noexcept
    {
        std::free(m_array);
    }

    type& operator[](size_type index) noexcept
    {
        return m_array[index];
    }

    type const& operator[](size_type index) const noexcept
    {
        return m_array[index];
    }

    size_type GetSize() const noexcept
    {
        return m_size;
    }

    size_type GetAllocSize() const noexcept
    {
        return m_alloc_size;
    }

    size_type GetGrowBy() const noexcept
    {
        return m_grow_by;
    }

    void SetGrowBy(size_type grow_by) noexcept
    {
        m_grow_by = grow_by;
    }

    void Clear() noexcept
    {
        std::free(m_array);
        m_array = nullptr;
        m_size = 0;
        m_alloc_size = 0;
    }

    bool Append(type const& value) noexcept
    {
        if (!Allocate(m_size + 1))
            return false;
        m_array[m_size] = value;
        ++m_size;
        return true;
    }

    bool Append(type&& value) noexcept
    {
        if (!Allocate(m_size + 1))
            return false;
        m_array[m_size] = static_cast<type&&>(value);
        ++m_size;
        return true;
    }

    void Move(ArgParseArray<type, size_type>& ar) noexcept
    {
        std::free(m_array);

        m_size = ar.m_size;
        m_alloc_size = ar.m_alloc_size;
        m_grow_by = ar.m_grow_by;
        m_array = ar.m_array;

        ar.m_array = nullptr;
        ar.m_size = 0;
        ar.m_alloc_size = 0;
        ar.m_grow_by = 1;
    }

private:
    /// Expand existing allocation with new space at end of array.
    bool Allocate(size_type size) noexcept
    {
        size_type alloc_size = m_alloc_size;
        if (size > alloc_size)
        {
            while (alloc_size < size)
                alloc_size += m_grow_by;
            type* ar = static_cast<type*>(std::malloc(sizeof(type) * (alloc_size)));
            if (ar == nullptr)
                return false;
            if (m_array != nullptr && m_size > 0)
                ::memcpy(ar, m_array, sizeof(type) * (m_size));
            m_alloc_size = alloc_size;
            std::free(m_array);
            m_array = ar;
        }
        return true;
    }

private:
    size_type m_size;
    size_type m_alloc_size;
    size_type m_grow_by;
    type* m_array;
};

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGPARSEARRAY_HPP

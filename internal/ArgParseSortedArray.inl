/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

template<typename CharType>
ArgParseSortedArray<CharType>::ArgParseSortedArray(int argc, char_type** argv) noexcept
    : m_argc(0)
    , m_argv(nullptr)
    , m_argv_array_size(0)
    , m_sorted_argv_array(nullptr)
    , m_error_position(InvalidPosition)
    , m_unsorted_position(1)
{
    if (argc > 0 && argv != nullptr)
        (void)Reset(argc, argv);
}

template<typename CharType>
ArgParseSortedArray<CharType>::~ArgParseSortedArray() noexcept
{
    std::free(m_sorted_argv_array);
}

template<typename CharType>
int ArgParseSortedArray<CharType>::GetArgc() const noexcept
{
    return m_argc;
}

template<typename CharType>
CharType** ArgParseSortedArray<CharType>::GetArgv() noexcept
{
    return m_argv;
}

template<typename CharType>
typename ArgParseSortedArray<CharType>::size_type
ArgParseSortedArray<CharType>::GetSize() const noexcept
{
    return m_argv_array_size;
}

template<typename CharType>
CharType const* ArgParseSortedArray<CharType>::GetAt(size_type index) const noexcept
{
    return index < m_argv_array_size ? m_sorted_argv_array[index] : nullptr;
}

template<typename CharType>
bool ArgParseSortedArray<CharType>::Reset(int argc, char_type** argv) noexcept
{
    m_argc = argc;
    m_argv = argv;

    m_argv_array_size = 0;
    std::free(m_sorted_argv_array);
    m_sorted_argv_array = static_cast<char_type const**>(
        std::malloc(static_cast<std::size_t>(argc - 1) * sizeof(char_type const**)));

    m_error_position = InvalidPosition;

    m_unsorted_position = 1;

    if (m_sorted_argv_array != nullptr)
    {
        for (int arg = 1; arg < argc; ++arg)
            if (!Insert(argv[arg]))
            {
                m_error_position = static_cast<size_type>(arg);
                break;
            }

        if (m_error_position != InvalidPosition)
        {
            std::free(m_sorted_argv_array);
            m_sorted_argv_array = nullptr;
        }
        else
            m_argv_array_size = argc - 1;
    }

    return m_error_position == InvalidPosition;
}

/// Return the matched option and remove from internal sorted list.
template<typename CharType>
CharType const* ArgParseSortedArray<CharType>::Remove(char_type const* option,
                                                      size_type option_len) noexcept
{
    if (m_error_position == InvalidPosition)
    {
        size_type nearest_position = InvalidPosition;
        size_type found_position = FindPosition(nearest_position, option, option_len);
        if (found_position != InvalidPosition)
            return RemoveByPosition(found_position);
    }
    return nullptr;
}

/// Return the matched option and remove from internal sorted list.
template<typename CharType>
CharType const* ArgParseSortedArray<CharType>::RemoveByPosition(size_type position) noexcept
{
    CharType const* found = nullptr;
    if (m_error_position == InvalidPosition && m_argv_array_size > 0)
    {
        if (position < m_argv_array_size - 1)
        {
            found = m_sorted_argv_array[position];
            ::memmove(m_sorted_argv_array + position,
                        m_sorted_argv_array + position + 1,
                        (m_argv_array_size - position) * sizeof(char_type*));
            --m_argv_array_size;
        }
        else if (position == m_argv_array_size - 1)
        {
            found = m_sorted_argv_array[position];
            --m_argv_array_size;
        }
    }
    return found;
}

template<typename CharType>
CharType const* ArgParseSortedArray<CharType>::GetUnsorted() noexcept
{
    while (static_cast<int>(m_unsorted_position) < m_argc)
    {
        char_type const* arg = m_argv[m_unsorted_position];
        if (arg != nullptr)
        {
            ++m_unsorted_position;
            if (*arg != char_constants_type::Hyphen)
                return arg;
        }
        else
            break;
    }
    return nullptr;
}

template<typename CharType>
bool ArgParseSortedArray<CharType>::Exists(char_type const* option, size_type option_len)
{
    size_type nearest_position = InvalidPosition;
    return FindPosition(nearest_position, option, option_len) != InvalidPosition;
}

/// Find the position for the matching option, otherwise return InvalidPosition
/// and set nearest position.
template<typename CharType>
typename ArgParseSortedArray<CharType>::size_type
ArgParseSortedArray<CharType>::FindPosition(size_type& nearest_position,
                                            char_type const* option,
                                            size_type option_len) noexcept
{
    bool found = FindPosition(nearest_position, m_sorted_argv_array, m_argv_array_size, option, option_len);
    return found ? nearest_position : InvalidPosition;
}

template<typename CharType>
bool ArgParseSortedArray<CharType>::HasError() const noexcept
{
    return m_error_position != InvalidPosition;
}

template<typename CharType>
typename ArgParseSortedArray<CharType>::size_type
ArgParseSortedArray<CharType>::GetErrorPosition() const noexcept
{
    return m_error_position;
}

template<typename CharType>
bool ArgParseSortedArray<CharType>::Insert(char_type const* option,
                                           size_type option_len) noexcept
{
    size_type nearest_position = InvalidPosition;
    if (m_error_position == InvalidPosition)
    {
        size_type insert_pos = FindPosition(nearest_position, option, option_len);
        if (insert_pos == InvalidPosition)
        {
            if (m_argv_array_size == 0)
                m_sorted_argv_array[0] = option; // First item always at the front.
            else
            {
                insert_pos = nearest_position;
                if (insert_pos < m_argv_array_size)
                {
                    // Make space for the insert before adding.
                    std::memmove(m_sorted_argv_array + insert_pos + 1,
                                 m_sorted_argv_array + insert_pos,
                                 (m_argv_array_size - insert_pos) * sizeof(char_type*));
                    m_sorted_argv_array[insert_pos] = option;
                }
                else
                    m_sorted_argv_array[m_argv_array_size] = option; // Append to end.
            }
            ++m_argv_array_size;
            return true;
        }
    }

    return false;
}

template<typename CharType>
typename ArgParseSortedArray<CharType>::size_type
ArgParseSortedArray<CharType>::GetOptionLength(char_type const* option) noexcept
{
    size_type option_len = string_utility_type::Find(option, char_constants_type::Equal);
    if (option_len == string_utility_type::InvalidPosition)
        option_len = string_utility_type::UnsafeGetLength(option);
    return option_len;
}

template<typename CharType>
bool ArgParseSortedArray<CharType>::FindPosition(size_type& nearest_position,
                                                 char_type const** sorted_array,
                                                 size_type array_size,
                                                 char_type const* option,
                                                 size_type option_len) noexcept
{
    bool found;

    if ((array_size > static_cast<size_type>(0)) && (sorted_array != nullptr) && (option != nullptr))
    {
        if (option_len == InvalidPosition)
            option_len = GetOptionLength(option);

        size_type start = 0;
        size_type end = array_size - static_cast<size_type>(1);
        size_type mid;
        int cmp;

        do
        {
            mid = (start + end) >> static_cast<size_type>(1);
            char_type const* to_compare = sorted_array[mid];
            cmp = string_utility_type::UnsafeCompare(option, to_compare, option_len);
            if (cmp < 0)
            {
                if (mid > 0)
                    end = mid - static_cast<size_type>(1);
                else
                    break;
            }
            else if (cmp > 0)
                start = mid + static_cast<size_type>(1);
            else
                break;
        } while (start <= end);

        mid = (start + end) >> static_cast<size_type>(1);
        size_type const offset = cmp > 0 ? 1 : 0;
        nearest_position = mid + offset;
        found = cmp == 0;
    }
    else
    {
        nearest_position = static_cast<size_type>(0);
        found = false;
    }

    return found;
}

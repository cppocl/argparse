/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGSTRINGTOINT_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGSTRINGTOINT_HPP

#include <cstdlib>
#include "ArgIntMinMax.hpp"
#include "ArgStringToLongLong.hpp"
#include "ArgInMinMaxRange.hpp"

namespace ocl
{

class ArgStringToInt
{
public:
    template<typename CharType, typename IntType>
    static bool ToInt(IntType& value, CharType const* str, int base = 10) noexcept
    {
        constexpr bool is_signed = static_cast<IntType>(-1) < 0;
        typedef ArgStringToLongLong <CharType, is_signed> str_to_long_long_type;
        typedef ArgIntMinMax<IntType> int_min_max_type;
        typedef typename str_to_long_long_type::long_long_type long_long_type;

        long_long_type long_value = 0;
        bool success = str_to_long_long_type::ToLongLong(long_value, str, base) &&
                       InMinMaxRange<long_long_type, IntType>(long_value);
        value = static_cast<IntType>(long_value);
        return success;
    }
};

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGSTRINGTOINT_HPP

/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGVEMULATION_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGVEMULATION_HPP

#include <cstring>
#include <cwchar>

namespace ocl
{

struct ArgvEmulation
{
    static char* Copy(const char* str)
    {
        char* dest = new char[std::strlen(str) + 1];
        std::strcpy(dest, str);
        return dest;
    }

    static wchar_t* Copy(wchar_t const* str)
    {
        wchar_t* dest = new wchar_t[std::wcslen(str) + 1];
        std::wcscpy(dest, str);
        return dest;
    }

    static char** CreateArgv(char const* str)
    {
        char** ar = new char*[2];
        ar[0] = nullptr; // argv[0] is not used.
        ar[1] = Copy(str);
        return ar;
    }

    static char** CreateArgv(char const* str1, char const* str2)
    {
        char** ar = new char*[3];
        ar[0] = nullptr; // argv[0] is not used.
        ar[1] = Copy(str1);
        ar[2] = Copy(str2);
        return ar;
    }

    static char** CreateArgv(char const* str1, char const* str2, char const* str3)
    {
        char** ar = new char*[4];
        ar[0] = nullptr; // argv[0] is not used.
        ar[1] = Copy(str1);
        ar[2] = Copy(str2);
        ar[3] = Copy(str3);
        return ar;
    }

    static char** CreateArgv(char const* str1, char const* str2, char const* str3, char const* str4)
    {
        char** ar = new char*[5];
        ar[0] = nullptr; // argv[0] is not used.
        ar[1] = Copy(str1);
        ar[2] = Copy(str2);
        ar[3] = Copy(str3);
        ar[4] = Copy(str4);
        return ar;
    }

    static void DeleteArgv(char** ar, std::size_t size)
    {
        for (std::size_t i = 0; i < size; ++i)
            delete[] ar[i];
        delete[] ar;
    }

    static wchar_t** CreateArgv(wchar_t const* str)
    {
        wchar_t** ar = new wchar_t*[2];
        ar[0] = nullptr; // argv[0] is not used.
        ar[1] = Copy(str);
        return ar;
    }

    static wchar_t** CreateArgv(wchar_t const* str1, wchar_t const* str2)
    {
        wchar_t** ar = new wchar_t*[3];
        ar[0] = nullptr; // argv[0] is not used.
        ar[1] = Copy(str1);
        ar[2] = Copy(str2);
        return ar;
    }

    static wchar_t** CreateArgv(wchar_t const* str1, wchar_t const* str2, wchar_t const* str3)
    {
        wchar_t** ar = new wchar_t*[4];
        ar[0] = nullptr; // argv[0] is not used.
        ar[1] = Copy(str1);
        ar[2] = Copy(str2);
        ar[3] = Copy(str3);
        return ar;
    }

    static wchar_t** CreateArgv(wchar_t const* str1, wchar_t const* str2, wchar_t const* str3, wchar_t const* str4)
    {
        wchar_t** ar = new wchar_t*[5];
        ar[0] = nullptr; // argv[0] is not used.
        ar[1] = Copy(str1);
        ar[2] = Copy(str2);
        ar[3] = Copy(str3);
        ar[4] = Copy(str4);
        return ar;
    }

    static void DeleteArgv(wchar_t** ar, std::size_t size)
    {
        for (std::size_t i = 0; i < size; ++i)
            delete[] ar[i];
        delete[] ar;
    }
};

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGVEMULATION_HPP

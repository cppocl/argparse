/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ARGPARSE_INTERNAL_ARGINMINMAXRANGE_HPP
#define OCL_GUARD_ARGPARSE_INTERNAL_ARGINMINMAXRANGE_HPP

#include "ArgIntMinMax.hpp"

namespace ocl
{

/// IntType1 is the type storing the value,
/// which needs to be within the minimum and maximum range of IntType2.
template<typename IntType1, typename IntType2>
inline bool InMinMaxRange(IntType1 value) noexcept
{
    typedef ArgIntMinMax<IntType2> int_min_max_type;

    bool const is_signed = static_cast<IntType1>(-1) < 0;
    constexpr bool is_smaller = sizeof(IntType1) < sizeof(IntType2);
    constexpr bool is_same = sizeof(IntType1) == sizeof(IntType2) && is_signed == int_min_max_type::IS_SIGNED;
    typename int_min_max_type::type const MIN_INT = int_min_max_type::MIN_INT;
    typename int_min_max_type::type const MAX_INT = int_min_max_type::MAX_INT;
    return is_smaller || is_same ? true : (is_signed ? value >= MIN_INT && value <= MAX_INT : value <= MAX_INT);
}

} // namespace ocl

#endif // OCL_GUARD_ARGPARSE_INTERNAL_ARGINMINMAXRANGE_HPP

# C++ Argument Parser

![](header_image.jpg)

## Overview

C++ open source argument parser for function main.

## Details

* ArgParse handles - and -- arguments, as well as arguments not starting with -.
* If the argument starts with - then only one more character is expected to follow. e.g. -v
* If the argument starts with -- then two or more characters are expected to follow. e.g. --verbose
* If the argument starts with - or -- and specifies a value, then = must be used. e.g. --depth=2

The ArgParse class is optimized to re-use argc and argv, and internally there are no string copies.  
ArgParse uses Bind to retrieve arguments, and supports bool, all integer types and const char* types.  
Arguments beginning with - or -- must be extracted using Bind function first.  
Any arguments expected not starting with - or -- can be extracted last with the Bind function that ony requires a const char* parameter.  
  
Internally the Bind function is doing a binary searcn on a sorted array, which is why = must be used with option.  
Each argument is added to the sorted array, and arguments not starting with - or -- will be at the back end of the sorted array.  
It's not possible to store a value with the argument unless = is used, e.g. --depth=2, otherwise the two arguments would be split  
and separated as part of adding the arguments to the sorted array.

## Follow

Telegram messenger: https://t.me/cppocl

## Code Examples

```cpp
#include <argparse/ArgParse.hpp>

int main(int argc, char** argv)
{
    typedef ocl::ArgParse<char> arg_parse_type;
    typedef typename arg_parse_type::help_array_type help_array_type;

    arg_parse_type arg_parse(argc, argv);

    bool help = false;
    bool to_upper = false;
    bool to_lower = false;
    bool verbose = false;
    char const* dest = nullptr;
    char const* src = nullptr;

    char const* program_name = "arg_parse_example";
    char const* program_args = "<dest> <src>";
    char const* version = "Version: 1.0";
    char const* license = "License: Apache 2.0";
    char const* description = "Perform upper or lower case conversion of a file.";
    char const* example = "arg_parse_example output.txt input_sample.txt --upper --verbose";

    arg_parse.AddHelp(program_name, program_args, version, license, description, example);

    arg_parse.Bind(help, "-?", "--help", "Show this help");
    arg_parse.Bind(to_upper, nullptr, "--upper", "Convert file to uppercase");
    arg_parse.Bind(to_lower, nullptr, "--lower", "Convert file to lowercase");
    arg_parse.Bind(verbose,  nullptr, "--verbose", "Output statistics about conversion");

    arg_parse.Bind(dest);
    arg_parse.Bind(src);

    char const* invalid_arg = arg_parse.GetInvalidArgument();

    if (help)
    {
        help_array_type const& help_array = arg_parse.GetHelp();
        for (std::size_t index = 0; index < help_array.GetSize(); ++index)
            std::cout << help_array[index] << std::endl;
    }
    else if (invalid_arg != nullptr)
        std::cout << "Invalid argument: " << invalid_arg << std::endl;
    else if (!to_upper && !to_lower)
        std::cout << "--upper or --lower required" << std::endl;
    else
        /* DO your code here... */;
}
```

## Help Example

```
Version: 1.0

License: Apache 2.0

Perform upper or lower case conversion of a file.

arg_parse_example <dest> <src>

-?  --help     Show this help
    --upper    Convert file to uppercase
    --lower    Convert file to lowercase
    --verbose  Output statistics about conversion

arg_parse_example output.txt input_sample.txt --upper --verbose
```

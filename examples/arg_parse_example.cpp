/*
Copyright 2020 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../ArgParse.hpp"
#include <cstdio>
#include <cctype>
#include <iostream>

void ToUpperLower(char const* dest, char const* src, bool to_upper, bool verbose)
{
    std::size_t converted_count = 0;
    std::size_t character_count = 0;

    std::cout << "Opening: " << src << std::endl;

    FILE* pf_read = fopen(src, "r");
    if (pf_read != nullptr)
    {
        FILE* pf_write = fopen(dest, "w");
        if (pf_write != nullptr)
        {
            std::cout << "FILE FOUND!" << std::endl;
            int c = fgetc(pf_read);
            while (c != EOF)
            {
                ++character_count;
                int c2 = to_upper ? toupper(c) : tolower(c);
                if (c != c2)
                    ++converted_count;
                if (fputc(c2, pf_write) == EOF)
                    break;
                c = fgetc(pf_read);
            }
            fclose(pf_write);
        }
        fclose(pf_read);
    }
    else
        std::cout << "FILE NOT FOUND!" << std::endl;

    if (verbose)
    {
        std::cout << "VERBOSE" << std::endl;
        std::cout << "Converted characters: " << converted_count << std::endl;
        std::cout << "Total characters: " << character_count << std::endl;
    }
    else
        std::cout << "NOT VERBOSE" << std::endl;
}

int main(int argc, char** argv)
{
    typedef ocl::ArgParse<char> arg_parse_type;
    typedef typename arg_parse_type::help_array_type help_array_type;

    arg_parse_type arg_parse(argc, argv);

    bool help = false;
    bool to_upper = false;
    bool to_lower = false;
    bool verbose = false;
    char const* dest = nullptr;
    char const* src = nullptr;

    char const* program_name = "arg_parse_example";
    char const* program_args = "<dest> <src>";
    char const* version = "Version: 1.0";
    char const* license = "License: Apache 2.0";
    char const* description = "Perform upper or lower case conversion of a file.";
    char const* example = "arg_parse_example output.txt input_sample.txt --upper --verbose";

    arg_parse.AddHelp(program_name, program_args, version, license, description, example);

    arg_parse.Bind(help, "-?", "--help", "Show this help");
    arg_parse.Bind(to_upper, nullptr, "--upper", "Convert file to uppercase");
    arg_parse.Bind(to_lower, nullptr, "--lower", "Convert file to lowercase");
    arg_parse.Bind(verbose,  nullptr, "--verbose", "Output statistics about conversion");

    arg_parse.Bind(dest);
    arg_parse.Bind(src);

    char const* invalid_arg = arg_parse.GetInvalidArgument();

    if (help)
    {
        help_array_type const& help_array = arg_parse.GetHelp();
        for (std::size_t index = 0; index < help_array.GetSize(); ++index)
            std::cout << help_array[index] << std::endl;
    }
    else if (invalid_arg != nullptr)
        std::cout << "Invalid argument: " << invalid_arg << std::endl;
    else if (!to_upper && !to_lower)
        std::cout << "--upper or --lower required" << std::endl;
    else
    {
        std::cout << "Starting conversion..." << std::endl;
        ToUpperLower(dest, src, to_upper, verbose);
    }
}
